import {Component, Inject, OnInit} from '@angular/core';
import {RequestService} from '../../shared/services/request.service';
import {MAT_DIALOG_DATA} from '@angular/material';

declare var $: any;

@Component({
    selector: 'app-my-dialog',
    templateUrl: './my-dialog.component.html',
    styleUrls: ['./my-dialog.component.scss']
})
export class MyDialogComponent implements OnInit {
    id;

    // tslint:disable-next-line:no-shadowed-variable
    constructor(private RequestService: RequestService, @Inject(MAT_DIALOG_DATA) public data) {

    }

    ngOnInit() {
        this.id = this.data.id;
    }

    showNotification(message, type, from = 'top', align = 'right') {
        $.notify({
            icon: 'notifications',
            message: message

        }, {
            type: type,
            timer: 4000,
            placement: {
                from: from,
                align: align
            },
            template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon"' +
                ' role="alert">' +
                '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  ' +
                '<i class="material-icons">close</i></button>' +
                '<i class="material-icons" data-notify="icon">notifications</i> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" ' +
                'role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
    }

    deleteService() {
        this.RequestService.deleteSolicitud(this.id).subscribe(response => {
            this.showNotification(response.message, 'success');
            setTimeout(() => {
                location.reload();
            }, 2000)
        }, error1 => {
            this.showNotification(error1.error.message, 'danger');
        })
    }
}
