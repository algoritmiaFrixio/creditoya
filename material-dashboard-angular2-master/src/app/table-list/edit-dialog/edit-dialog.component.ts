import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RequestService} from '../../shared/services/request.service';
import {TypeHouse} from '../../shared/interfaces/house/type-house';
import {ActivatedRoute, Router} from '@angular/router';
import {Request} from '../../shared/interfaces/request';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

declare var $: any;

@Component({
    selector: 'app-edit-dialog',
    templateUrl: './edit-dialog.component.html',
    styleUrls: ['./edit-dialog.component.scss']
})

export class EditDialogComponent implements OnInit {
    public solicitud: Request;
    public show = false;
    public houses: TypeHouse[];
    public firstFormGroup: FormGroup;
    public secondFormGroup: FormGroup;
    public fiveFormGroup: FormGroup;
    public sixFormGroup: FormGroup;
    public equalAddress = false;

    constructor(private route: ActivatedRoute, private formBuilder: FormBuilder, private requestService: RequestService, private router: Router,
                public dialogRef: MatDialogRef<EditDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data
    ) {
        if (localStorage.getItem(btoa('auth')) === null) {
            this.router.navigate(['/login']);
        }
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    public change(direccion1, direccion2) {
        setTimeout(() => {
            this.equalAddress = (direccion1 === direccion2);
            if (this.equalAddress) {

            }
        });
    }


    showNotification(message, type, from = 'top', align = 'right') {
        // const type = ['', 'info', 'success', 'warning', 'danger'];
        //
        // const color = Math.floor((Math.random() * 4) + 1);

        $.notify({
            icon: 'notifications',
            message: message

        }, {
            type: type,
            timer: 4000,
            placement: {
                from: from,
                align: align
            },
            template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon"' +
                ' role="alert">' +
                '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  ' +
                '<i class="material-icons">close</i></button>' +
                '<i class="material-icons" data-notify="icon">notifications</i> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" ' +
                'role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
    }

    editService() {
        const data = {
            personal: {},
            localizacion: {},
            laboral: {},
            referencia1: {},
            referencia2: {},
            token: this.requestService.token,
        };
        data.personal = this.firstFormGroup.value;
        data.localizacion = this.secondFormGroup.value;
        data.laboral = this.sixFormGroup.value;
        data.referencia1 = {
            id: this.solicitud.references[0].id,
            nombre: this.fiveFormGroup.value.nombre,
            direccion: this.fiveFormGroup.value.direccion,
            telefono: this.fiveFormGroup.value.telefono,
            ciudad: this.fiveFormGroup.value.ciudad
        };
        data.referencia2 = {
            id: this.solicitud.references[1].id,
            nombre: this.fiveFormGroup.value.nombre1,
            direccion: this.fiveFormGroup.value.direccion1,
            telefono: this.fiveFormGroup.value.telefono1,
            ciudad: this.fiveFormGroup.value.ciudad1,
        };
        this.requestService.editSolicitud(this.solicitud.id, data).subscribe(response => {
            this.showNotification(response.message, 'success');
            setTimeout(() => {
                location.reload();
            }, 2000)
        }, error1 => {
            this.showNotification(error1.error.message, 'danger');
        })
    }


    ngOnInit() {
        this.solicitud = this.data;

        this.requestService.getHouse().subscribe((response: TypeHouse[]) => {
            this.houses = response;
        }, error1 => {
            this.showNotification(error1.error.message, 'danger');
        });
        this.firstFormGroup = this.formBuilder.group({
            nombre: [this.solicitud.nombre, [Validators.required, Validators.pattern(/^([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\']+[\s])+([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])+[\s]?([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])?$/)]],
            documento: [this.solicitud.documento, [Validators.required, Validators.pattern(/^([0-9])*$/)]],
            lugar_expedicion: [this.solicitud.lugar_expedicion, [Validators.required]],
            fecha_nacimiento: [this.solicitud.fecha_nacimiento, [Validators.required]],
            fecha_expedicion: [this.solicitud.fecha_expedicion, [Validators.required]],
        });
        this.secondFormGroup = this.formBuilder.group({
            direcion: [this.solicitud.direcion, [Validators.required]],
            barrio: [this.solicitud.barrio, [Validators.required]],
            house_type_id: [this.solicitud.house_type_id, [Validators.required, Validators.pattern(/^([0-9])*$/)]],
            telefono: [this.solicitud.telefono, [Validators.required, Validators.min(7)]],
            correo: [this.solicitud.correo, [Validators.pattern(/^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/)]],
        });
        this.fiveFormGroup = this.formBuilder.group({
            nombre: [this.solicitud.references[0].nombre, [Validators.required, Validators.pattern(/^([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\']+[\s])+([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])+[\s]?([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])?$/)]],
            direccion: [this.solicitud.references[0].direccion, [Validators.required]],
            telefono: [this.solicitud.references[0].telefono, [Validators.required, Validators.min(7)]],
            ciudad: [this.solicitud.references[0].ciudad, [Validators.required]],
            nombre1: [this.solicitud.references[1].nombre, [Validators.required, Validators.pattern(/^([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\']+[\s])+([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])+[\s]?([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])?$/)]],
            direccion1: [this.solicitud.references[1].direccion, [Validators.required]],
            telefono1: [this.solicitud.references[1].telefono, [Validators.required, Validators.min(7)]],
            ciudad1: [this.solicitud.references[1.].ciudad, [Validators.required]],

        });

        this.sixFormGroup = this.formBuilder.group({
            salario: [this.solicitud.salario, [Validators.required, Validators.pattern(/^([0-9])*$/)]],
            profesion: [this.solicitud.profesion, [Validators.required]],
            empresa: [this.solicitud.empresa, [Validators.required]],
            cargo: [this.solicitud.cargo, [Validators.required]],
            direccion_empresa: [this.solicitud.direcion_empresa, [Validators.required]],
            telefono_trabajo: [this.solicitud.telefono_trabajo, [Validators.required, Validators.min(7)]],
            otros_ingresos: ['', [Validators.pattern(/^([0-9])*$/)]],
            procedencia: [this.solicitud.procedencia],
            actividad_independiente: [this.solicitud.actividad_independiente, [Validators.required]],
        })
    }
}
