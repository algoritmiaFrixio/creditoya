import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RequestService} from '../shared/services/request.service';
import {Request} from '../shared/interfaces/request';
import {MyDialogComponent} from './my-dialog/my-dialog.component';
import {EditDialogComponent} from './edit-dialog/edit-dialog.component';
import {MatDialog} from '@angular/material';

@Component({
    selector: 'app-table-list',
    templateUrl: './table-list.component.html',
    styleUrls: ['./table-list.component.css']
})

export class TableListComponent implements OnInit {
    public solicituds: Request[] = [];
    public message = 'Todos';
    public message1 = 'En esta lista se encuentran todas las solicitudes';
    private optionsF = {year: 'numeric', month: 'long', day: 'numeric'};

    constructor(public dialog: MatDialog, private route: ActivatedRoute, private requestService: RequestService, private router: Router,) {
        if (localStorage.getItem(btoa('auth')) === null) {
            this.router.navigate(['/login']);
        }
        this.route.params.subscribe(params => {
            const id = params['tipo'];
            const ano = params['ano'];
            this.message = id;
            const msg = ano !== 'todos' ? `en el año ${ano}` : '';
            if (id !== 'todos') {
                this.message1 = 'En esta lista se encuentran las solicitudes';
                switch (id) {
                    case 'Aprobado':
                        this.message1 += ' aprobadas ' + msg;
                        break;
                    case 'Rechazado':
                        this.message1 += 'Rechazadas ' + msg;
                }
            }
            requestService.getSolicituds(ano).subscribe((response: Request[]) => {
                if (id !== 'todos') {
                    response.forEach(e => {
                        if (e.estado === id) {
                            this.solicituds.push(e);
                        }
                    });
                } else {
                    this.solicituds = response
                }
                this.solicituds.forEach(e => {
                    e.created_at = new Date(e.created_at).toLocaleDateString('es-ES', this.optionsF);
                });
            });
        });
    }


    openDialog(id): void {
        const dialogRef = this.dialog.open(MyDialogComponent, {
            width: '65vh',
            disableClose: true,
            data: {id}
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    opendialog1(id): void {
        this.requestService.getSolicitud('todos', id).subscribe((response: Request) => {
            const dialogRef = this.dialog.open(EditDialogComponent, {
                width: '700vw',
                height: '87vh',
                disableClose: true,
                data: response
            });
            dialogRef.afterClosed().subscribe(result => {
                console.log('The dialog was closed');

            });
        });
    }


    ngOnInit() {
    }

}
