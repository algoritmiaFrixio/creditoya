import {Routes} from '@angular/router';

import {DashboardComponent} from '../../dashboard/dashboard.component';
import {TableListComponent} from '../../table-list/table-list.component';
import {NotificationsComponent} from '../../notifications/notifications.component';

export const AdminLayoutRoutes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {path: 'dashboard', component: DashboardComponent},
    {path: 'solicitudes/:tipo/:ano', component: TableListComponent},
    {
        path: 'solicitudes',
        redirectTo: 'solicitudes/todos/todos'
    },
    {path: 'view/:id', component: NotificationsComponent},
    {
        path: '**',
        redirectTo: 'dashboard'
    }
];
