import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {MatButtonModule} from '@angular/material/button';
import {AppRoutingModule} from './app.routing';
import {ComponentsModule} from './components/components.module';
import {MatDialogModule} from '@angular/material/dialog';
import {AppComponent} from './app.component';
import {AdminLayoutComponent} from './layouts/admin-layout/admin-layout.component';
import {HttpClientModule} from '@angular/common/http';
import {UpgradeComponent} from './upgrade/upgrade.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {ChartsModule} from 'ng2-charts';
import {NgxUiLoaderModule, NgxUiLoaderRouterModule, NgxUiLoaderHttpModule} from 'ngx-ui-loader';
import {MyDialogComponent} from './table-list/my-dialog/my-dialog.component';
import {EditDialogComponent} from './table-list/edit-dialog/edit-dialog.component';
import {MatToolbarModule} from '@angular/material';
import {
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatStepperModule,
} from '@angular/material';
import {MessagingService} from './shared/services/messaging.service';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';


@NgModule({
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        ComponentsModule,
        RouterModule,
        ChartsModule,
        MatButtonModule,
        MatDialogModule,
        AppRoutingModule,
        MatToolbarModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSelectModule,
        MatSlideToggleModule,
        MatStepperModule,
        NgxUiLoaderRouterModule.forRoot({showForeground: true}),
        NgxUiLoaderHttpModule.forRoot({showForeground: true}),
        NgxUiLoaderModule,
        ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production}),
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        AngularFireMessagingModule,
        AngularFireModule.initializeApp(environment.firebase),

    ],
    declarations: [
        AppComponent,
        AdminLayoutComponent,
        UpgradeComponent,
        MyDialogComponent,
        EditDialogComponent,
    ],
    entryComponents: [
        MyDialogComponent,
        EditDialogComponent,
    ],
    providers: [MatDatepickerModule, MessagingService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
