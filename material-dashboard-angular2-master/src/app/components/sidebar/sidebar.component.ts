import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';

declare const $: any;

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES: RouteInfo[] = [
    {path: '/admin/dashboard', title: 'Dashboard', icon: 'dashboard', class: ''},
    {path: '/admin/solicitudes', title: 'Solicitudes', icon: 'content_paste', class: ''},
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    menuItems: any[];

    constructor(private router: Router, public dialog: MatDialog) {
    }


    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }

    public logOut() {
        localStorage.clear();
        this.router.navigate(['/login']);
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
}
