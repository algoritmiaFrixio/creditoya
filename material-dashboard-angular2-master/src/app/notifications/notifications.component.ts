import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RequestService} from '../shared/services/request.service';
import {Request} from '../shared/interfaces/request';

declare var $: any;

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
    public solicitud: Request;
    public estado = 'En espera';
    public show = false;
    private optionsF = {year: 'numeric', month: 'long', day: 'numeric'};

    constructor(private route: ActivatedRoute, private requestService: RequestService, private router: Router) {
        if (localStorage.getItem(btoa('auth')) === null) {
            this.router.navigate(['/login']);
        }
        this.route.params.subscribe(params => {
            const id = params['id'];
            requestService.getSolicitud('todos', id).subscribe((response: Request) => {
                response.created_at = new Date(response.created_at).toLocaleDateString('es-ES', this.optionsF);
                this.solicitud = response;
                this.show = response.spouse !== null;
                this.estado = response.estado
            });
        });
    }

    public change() {
        this.requestService.changeStatus(this.estado, this.solicitud.id).subscribe(response => {
            this.showNotification(response.message, 'success');
        }, error1 => {
            this.showNotification(error1.error.message, 'danger');
        })
    }

    showNotification(message, type, from = 'top', align = 'right') {
        // const type = ['', 'info', 'success', 'warning', 'danger'];
        //
        // const color = Math.floor((Math.random() * 4) + 1);

        $.notify({
            icon: 'notifications',
            message: message

        }, {
            type: type,
            timer: 4000,
            placement: {
                from: from,
                align: align
            },
            template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon"' +
                ' role="alert">' +
                '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  ' +
                '<i class="material-icons">close</i></button>' +
                '<i class="material-icons" data-notify="icon">notifications</i> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" ' +
                'role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
    }

    ngOnInit() {
    }

}
