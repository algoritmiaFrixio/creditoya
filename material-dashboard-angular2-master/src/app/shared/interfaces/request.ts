export interface Request {
    actividad_independiente?: string;
    barrio?: string;
    cargo?: string;
    civil_status?: CivilStatus;
    civil_status_id?: number;
    correo?: string;
    direcion?: string;
    direcion_empresa?: string;
    documento?: string;
    empleado?: boolean;
    empresa?: string;
    estado?: string;
    fecha_expedicion?: string;
    fecha_nacimiento?: string;
    house_type?: HouseType;
    house_type_id?: number;
    id?: number;
    imagen_cedula_adelante?: string;
    imagen_cedula_atras?: string;
    imagen_persona?: string;
    lugar_expedicion?: string;
    nombre?: string;
    otros_ingresos?: number;
    procedencia?: string;
    profesion?: string;
    references?: Reference[];
    salario?: number;
    spouse?: Spouse;
    telefono?: string;
    telefono_trabajo?: string;
    tiempo_servicio?: string;
    type_document?: TypeDocument;
    type_document_id?: number;
    created_at?: string;
    vendedor?: string;
}

export interface CivilStatus {
    id?: number;
    nombre?: string;
}

export interface HouseType {
    id?: string;
    nombre?: string;
}

export interface Reference {
    ciudad?: string;
    direccion?: string;
    id?: number;
    nombre?: string;
    telefono: string;
    pivot?: Pivot;
}

export interface Pivot {
    request_id?: number;
    reference_id?: number;
}

export interface Spouse {
    clase_actividad?: string;
    id?: number;
    nombre_conyuge?: string;
    numero_documento_conyuge?: string;
    request_id?: number;
    salario?: number;
    telefono?: string;
    trabajo?: string;
}

export interface TypeDocument {
    id?: number;
    nombre?: string;
}
