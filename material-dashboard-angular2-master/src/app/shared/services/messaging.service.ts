import {Injectable} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFireMessaging} from '@angular/fire/messaging';
import {map, mergeMapTo} from 'rxjs/operators';
import {take} from 'rxjs/operators';
import {BehaviorSubject, Observable} from 'rxjs'
import {HttpClient, HttpResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';

declare var $: any;

@Injectable()
export class MessagingService {

    currentMessage = new BehaviorSubject(null);

    constructor(
        private angularFireDB: AngularFireDatabase,
        private angularFireAuth: AngularFireAuth,
        private angularFireMessaging: AngularFireMessaging,
        private http: HttpClient) {
        this.angularFireMessaging.messaging.subscribe(
            (_messaging) => {
                _messaging.onMessage = _messaging.onMessage.bind(_messaging);
                _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
            }
        )
    }

    /**
     * update token in firebase database
     *
     * @param userId userId as a key
     * @param token token as a value
     */
    updateToken(userId, token) {
        // we can change this function to request our backend service
        this.angularFireAuth.authState.pipe(take(1)).subscribe(
            () => {
                const data = {};
                data[userId] = token
                this.angularFireDB.object('fcmTokens/').update(data)
            })
    }

    /**
     * request permission for notification from firebase cloud messaging
     *
     * @param userId userId
     */
    requestPermission(userId) {
        this.angularFireMessaging.requestToken.subscribe(
            (token) => {
                this.updateToken(userId, token);
                this.updateTokens({id_user: userId, token: token}).subscribe(response => {
                    console.log( 'se ha actualizado')
                }, error => {
                    console.log(error, 'ha ocurrido un error')
                });
            },
            (err) => {
                console.error('Unable to get permission to notify.', err);
            }
        );
    }

    /**
     * hook method when new notification received in foreground
     */
    receiveMessage() {
        this.angularFireMessaging.messages.subscribe(
            (payload) => {
                console.log('new message received. ', payload);
                this.currentMessage.next(payload);
            })
    }

    updateTokens(data): Observable<any> {
        return this.http.post(`${environment.UrlServer}token`, data, {
            headers: {'Content-Type': 'application/json'},
            observe: 'response'
        }).pipe(map((response: HttpResponse<any>) => response.body));
    }

    showNotification(message, type, from = 'top', align = 'right') {
        // const type = ['', 'info', 'success', 'warning', 'danger'];
        //
        // const color = Math.floor((Math.random() * 4) + 1);
        $.notify({
                icon: 'notifications',
                message: message

            },
            {
                type: type,
                timer: 4000,
                placement: {
                    from: from,
                    align: align
                },
                // tslint:disable-next-line:max-line-length
                template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon"' +
                    ' role="alert">' +
                    '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  ' +
                    '<i class="material-icons">close</i></button>' +
                    '<i class="material-icons" data-notify="icon">notifications</i> ' +
                    '<span data-notify="title">{1}</span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" ' +
                    'role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'
            });
    }
}
