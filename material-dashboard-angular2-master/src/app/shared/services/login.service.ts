import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class LoginService {
    private _token = '';

    get token(): string {
        return this._token;
    }

    set token(value: string) {
        this._token = value;
    }
    constructor(private http: HttpClient) {
    }

    public getToken(): Observable<any> {
        return this.http.get<any>(`${environment.UrlServer}`, {
            headers: {'Content-Type': 'application/json'}
        }).pipe(map((response: any) => response));
    }

    public auth(credenciales): Observable<any> {
        return this.http.post<any>(`${environment.UrlServer}auth`, credenciales, {
            headers: {'Content-Type': 'application/json'},
        }).pipe(map((response: any) => response));
    }

}
