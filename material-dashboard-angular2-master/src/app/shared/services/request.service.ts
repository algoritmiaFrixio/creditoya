import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {Request} from '../interfaces/request';
import {TypeHouse} from '../interfaces/house/type-house';
import {observableToBeFn} from 'rxjs/internal/testing/TestScheduler';

@Injectable({
    providedIn: 'root'
})
export class RequestService {
    private _token = '';

    get token(): string {
        return this._token;
    }

    set token(value: string) {
        this._token = value;
    }

    constructor(private http: HttpClient) {
    }

    public getSolicituds(ano): Observable<Request[]> {
        return this.http.get<Request[]>(`${environment.UrlServer}request?token=${this.token}&ano=${ano}`, {
            headers: {'Content-Type': 'application/json'}
        }).pipe(map((response: Request[]) => response));
    }

    public getSolicitud(ano, id): Observable<Request> {
        return this.http.get<Request>(`${environment.UrlServer}request/${id}?token=${this.token}&ano=${ano}&id=${id}`, {
            headers: {'Content-Type': 'application/json'}
        }).pipe(map((response: Request) => response));
    }

    public changeStatus(status, id): Observable<any> {
        return this.http.put<any>(`${environment.UrlServer}request/${id}?token=${this.token}&status=${status}&id=${id}`, {
            headers: {'Content-Type': 'application/json'}
        }).pipe(map((response: any) => response));
    }

    public getToken(): Observable<any> {
        return this.http.get<any>(`${environment.UrlServer}`, {
            headers: {'Content-Type': 'application/json'}
        }).pipe(map((response: any) => response));
    }

    public deleteSolicitud(id): Observable<any> {
        return this.http.delete<any>(`${environment.UrlServer}request/id?token=${this.token}&id=${id}`,
            {
                headers: {'Content-Type': 'application/json'}
            }).pipe(map((response: any) => response));
    }

    public getHouse(): Observable<TypeHouse[]> {
        return this.http.get<TypeHouse[]>(`${environment.UrlServer}tipo-casa?token=${this._token}`, {
            headers: {'Content-Type': 'application/json'}
        }).pipe(map((response: TypeHouse[]) => response));
    }

    public getCivil(): Observable<any[]> {
        return this.http.get<any[]>(`${environment.UrlServer}estado-civil?token=${this._token}`, {
            headers: {'Content-Type': 'application/json'}
        }).pipe(map((response: any[]) => response));
    }

    public editSolicitud(id, data): Observable<any> {
        data.id = id;
        data.token = this.token;
        return this.http.post<any>(`${environment.UrlServer}update`, data,
            {
                headers: {'Content-Type': 'application/json'}
            }).pipe(map((response: any) => response));
    }

}
