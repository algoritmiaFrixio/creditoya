import {Component, OnInit} from '@angular/core';
import {RequestService} from '../shared/services/request.service';
import {Request} from '../shared/interfaces/request';
import {Router} from '@angular/router';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Label} from 'ng2-charts';
import {MessagingService} from '../shared/services/messaging.service';

declare var $: any;

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    message;
    public total = 0;
    public arratTotal = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    public totalEspera = 0;
    public arrayEspera = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    public totalAprobado = 0;
    public arrayAprobado = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    public totalNegado = 0;
    public arrayNegado = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    public anoCurrent = new Date().getFullYear();
    public anoArray = ['todos'];
    public ano = 'todos';
    public barChartOptions: ChartOptions = {
        responsive: true,
    };
    public barChartLabels: Label[] = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    public barChartType: ChartType = 'bar';
    public barChartLegend = true;
    public barChartPlugins = [];
    public chartColors: Array<any> = [
        { // first color
            backgroundColor: 'rgba(0, 188, 212, 0.3)',
            borderColor: 'rgba(0, 188, 212, 1)',
            pointBackgroundColor: 'rgba(0, 188, 212, 0.3)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(0, 188, 212, 0.3)'
        },
        { // second color
            backgroundColor: 'rgba(255, 152, 0, 0.3)',
            borderColor: 'rgba(255, 152, 0, 1)',
            pointBackgroundColor: 'rgba(255, 152, 0, 0.3)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(255, 152, 0, 0.3)'
        },
        { // second color
            backgroundColor: 'rgba(76, 175, 80, 0.3)',
            borderColor: 'rgba(76, 175, 80, 1)',
            pointBackgroundColor: 'rgba(76, 175, 80, 0.3)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(76, 175, 80, 0.3)'
        },
        { // second color
            backgroundColor: 'rgba(244, 67, 54, 0.3)',
            borderColor: 'rgba(244, 67, 54, 1)',
            pointBackgroundColor: 'rgba(244, 67, 54, 0.3)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(244, 67, 54, 0.3)'
        }];
    public barChartData: ChartDataSets[] = [
        {
            data: this.arratTotal, label: 'Todos'
        },
        {
            data: this.arrayEspera, label: 'En espera'
        },
        {
            data: this.arrayAprobado, label: 'Aprobado'
        },
        {
            data: this.arrayNegado, label: 'Rechazado'
        }
    ];

    constructor(private requestService: RequestService, private router: Router, private messagingService: MessagingService) {
        if (localStorage.getItem(btoa('auth')) === null) {
            this.router.navigate(['/login']);
        }
        this.getRequest('todos');
        for (let i = 2019; i <= this.anoCurrent; i++) {
            this.anoArray.push(i.toString());
        }
    }

    public change() {
        this.total = 0;
        this.arratTotal = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        this.totalEspera = 0;
        this.arrayEspera = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        this.totalAprobado = 0;
        this.arrayAprobado = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        this.totalNegado = 0;
        this.arrayNegado = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        setTimeout(() => {
            this.getRequest(this.ano);
        });
    }

    public getRequest(ano) {
        this.requestService.getSolicituds(ano).subscribe((response: Request[]) => {
            this.total = response.length;
            response.forEach(e => {
                this.arratTotal[new Date(e.created_at).getMonth()]++;
                switch (e.estado) {
                    case 'En espera':
                        this.totalEspera++;
                        this.arrayEspera[new Date(e.created_at).getMonth()]++;
                        break;
                    case 'Aprobado':
                        this.totalAprobado++;
                        this.arrayAprobado[new Date(e.created_at).getMonth()]++;
                        break;
                    case 'Rechazado':
                        this.totalNegado++;
                        this.arrayNegado[new Date(e.created_at).getMonth()]++;
                        break;
                }
            });
            this.barChartData = [
                {data: this.arratTotal, label: 'Todos'},
                {data: this.arrayEspera, label: 'En espera'},
                {data: this.arrayAprobado, label: 'Aprobado'},
                {data: this.arrayNegado, label: 'Rechaado'}];
        });
    }

    showNotification(message, type, from = 'top', align = 'right') {
        // const type = ['', 'info', 'success', 'warning', 'danger'];
        //
        // const color = Math.floor((Math.random() * 4) + 1);

        $.notify({
            icon: 'notifications',
            message: message

        },
            {
            type: type,
            timer: 4000,
            placement: {
                from: from,
                align: align
            },
            template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon"' +
                ' role="alert">' +
                '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  ' +
                '<i class="material-icons">close</i></button>' +
                '<i class="material-icons" data-notify="icon">notifications</i> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" ' +
                'role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
    }

    ngOnInit() {
        this.messagingService.requestPermission(JSON.parse(atob(localStorage.getItem('YXV0aA=='))).user.id);
        this.messagingService.receiveMessage();
        this.message = this.messagingService.currentMessage;
    }
}
