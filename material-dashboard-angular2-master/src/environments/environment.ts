// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    UrlServer: 'https://creditoapi.almaceneselpunto.com/api-creditoya/public/index.php/api/',
    firebase: {
        apiKey: 'AIzaSyDoGle_WuVRVKwZSYzJFXEi1oj997emXJE',
        authDomain: 'creditoya-e84c1.firebaseapp.com',
        databaseURL: 'https://creditoya-e84c1.firebaseio.com',
        projectId: 'creditoya-e84c1',
        storageBucket: 'creditoya-e84c1.appspot.com',
        messagingSenderId: '320625180575',
        appId: '1:320625180575:web:8ac3008b002e024b'
    }
};
