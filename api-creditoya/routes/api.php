<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('documento', 'TypeDocumentController');
Route::apiResource('estado-civil', 'CivilStatusController');
Route::apiResource('request', 'SolicitudController');
Route::post('update', 'SolicitudController@updateRequest');
Route::post('auth', 'UserController@auth');
Route::apiResource('tipo-casa', 'HouseTypeController');
Route::apiResource('token', 'TokensController');
Route::get('', 'SolicitudController@inicio');
Route::get('addUsers', 'UserController@addUsers');
