<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicituds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 100);
            $table->unsignedBigInteger('type_document_id');
            $table->foreign('type_document_id')
                ->references('id')
                ->on('type_documents')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('documento', 20);
            $table->string('lugar_expedicion', 50);
            $table->date('fecha_nacimiento');
            $table->unsignedBigInteger('civil_status_id');
            $table->foreign('civil_status_id')
                ->references('id')
                ->on('civil_statuses')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('direcion', 50);
            $table->string('barrio', 30);
            $table->unsignedBigInteger('house_type_id');
            $table->foreign('house_type_id')
                ->references('id')
                ->on('house_types')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('telefono', 30);
            $table->string('correo', 50)->nullable();
            $table->boolean('empleado');
            $table->string('empresa', 40)->nullable();
            $table->string('profesion', 30)->nullable();
            $table->string('telefono_trabajo', 30)->nullable();
            $table->string('direcion_empresa', 30)->nullable();
            $table->float('salario');
            $table->string('cargo', 30)->nullable();
            $table->date('tiempo_servicio')->nullable();
            $table->date('fecha_expedicion');
            $table->string('actividad_independiente', 30)->nullable();
            $table->float('otros_ingresos')->nullable();
            $table->string('procedencia', 30)->nullable();
            $table->string('vendedor', 30)->nullable();
            $table->enum('estado', ['Aprobado', 'Rechazado', 'En espera'])->default('En espera');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicituds');
    }
}
