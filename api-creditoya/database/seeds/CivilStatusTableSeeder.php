<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CivilStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('civil_statuses')->insert([
            'id' => 1,
            'nombre' => 'Soltero(a)'
        ]);
        DB::table('civil_statuses')->insert([
            'id' => 2,
            'nombre' => 'Casado(a)'
        ]);
        DB::table('civil_statuses')->insert([
            'id' => 3,
            'nombre' => 'Viudo(a)'
        ]);
        DB::table('civil_statuses')->insert([
            'id' => 4,
            'nombre' => 'Comprometido(a)'
        ]);
        DB::table('civil_statuses')->insert([
            'id' => 5,
            'nombre' => 'Separado(a)'
        ]);
        DB::table('civil_statuses')->insert([
            'id' => 6,
            'nombre' => 'Divorciado(a)'
        ]);
        DB::table('civil_statuses')->insert([
            'id' => 7,
            'nombre' => 'Union libre'
        ]);
        DB::table('civil_statuses')->insert([
            'id' => 8,
            'nombre' => 'En Relación'
        ]);
        DB::table('users')->insert([
            'usuario' => 'bordados',
            'contrasena' => Hash::make('creditoenlinea2019')
        ]);
        DB::table('users')->insert([
            'usuario' => 'cartera',
            'contrasena' => Hash::make('creditoenlinea2019')
        ]);
        DB::table('users')->insert([
            'usuario' => 'cartera1',
            'contrasena' => Hash::make('creditoenlinea2019')
        ]);

    }   
}
