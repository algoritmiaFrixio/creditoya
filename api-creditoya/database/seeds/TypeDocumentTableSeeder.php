<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeDocumentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_documents')->insert([
            'nombre' => 'C.C.'
        ]);
        DB::table('type_documents')->insert([
            'nombre' => 'NIT.'
        ]);
    }
}
