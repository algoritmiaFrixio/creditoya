<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HouseTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('house_types')->insert([
            'id' => 1,
            'nombre' => 'Propia'
        ]);
        DB::table('house_types')->insert([
            'id' => 2,
            'nombre' => 'Alquilada'
        ]);
        DB::table('house_types')->insert([
            'id' => 3,
            'nombre' => 'Casa Paterna'
        ]);
        //solicituds
        DB::table('solicituds')->insert([
            'id' => 40,
            'nombre' => 'Diver cifuentes',
            'type_document_id' => 1,
            'documento' => 94519030,
            'lugar_expedicion' => 'Cali Valle',
            'fecha_nacimiento' => new \Carbon\Carbon('1977-11-23'),
            'civil_status_id' => 2,
            'direcion' => 'Manzana 3 casa 16',
            'barrio' => 'El jasmin',
            'house_type_id' => 1,
            'telefono' => 3105013552,
            'correo' => 'julianafreitag1998@gmail.com',
            'empleado' => true,
            'empresa' => 'Negocio Independiente',
            'profesion' => 'Independiente',
            'telefono_trabajo' => 3105013552,
            'direcion_empresa' => 'Lea 3 #22- 88',
            'salario' => 12000000,
            'cargo' => 'Panadero',
            'tiempo_servicio' => new \Carbon\Carbon('2000-05-04'),
            'fecha_expedicion' => new \Carbon\Carbon('1996-10-02'),
            'otros_ingresos' => 500000,
            'procedencia' => 'Prestamos',
            'vendedor' => 'Diver',
            'estado' => 'Aprobado',
            'created_at' => new \Carbon\Carbon('2019-06-04 19:21:29'),
            'updated_at' => new \Carbon\Carbon('2019-06-05 14:17:33'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 43,
            'nombre' => 'Martha gutierrez',
            'type_document_id' => 1,
            'documento' => 38889321,
            'lugar_expedicion' => 'Cali valle',
            'fecha_nacimiento' => new \Carbon\Carbon('1979-06-07'),
            'civil_status_id' => 2,
            'direcion' => 'Carrera 1 norte#54-33',
            'barrio' => 'Alcazares',
            'house_type_id' => 1,
            'telefono' => 3017165725,
            'correo' => 'marthagutierrez08@hotmail.com',
            'empleado' => false,
            'salario' => 1000000,
            'fecha_expedicion' => new \Carbon\Carbon('1998-04-27'),
            'actividad_independiente' => 'Comersiante',
            'otros_ingresos' => 000,
            'procedencia' => ' No tengo otros ingresos',
            'vendedor' => 'El punto',
            'estado' => 'Aprobado',
            'created_at' => new \Carbon\Carbon('2019-06-05 22:45:00'),
            'updated_at' => new \Carbon\Carbon('2019-06-05 23:54:29'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 45,
            'nombre' => 'Juliana Freitag Marulanda',
            'type_document_id' => 1,
            'documento' => 1112792676,
            'lugar_expedicion' => 'Cartago Valle Del Ca',
            'fecha_nacimiento' => new \Carbon\Carbon('1998-12-05'),
            'civil_status_id' => 1,
            'direcion' => 'Cra 5 Anorte 16b 08',
            'barrio' => 'Prado norte',
            'house_type_id' => 1,
            'telefono' => 3128929539,
            'correo' => 'julianafreitag1998@gmail.com',
            'empleado' => false,
            'salario' => 600,
            'fecha_expedicion' => new \Carbon\Carbon('2016-12-06'),
            'otros_ingresos' => 600000100,
            'procedencia' => 'Venta de ropa por catálogo',
            'vendedor' => 'Pacho Perea',
            'estado' => 'Aprobado',
            'created_at' => new \Carbon\Carbon('2019-06-07 16:01:46'),
            'updated_at' => new \Carbon\Carbon('2019-06-07 17:19:14'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 47,
            'nombre' => 'Orfa yaneth correa patiño',
            'type_document_id' => 1,
            'documento' => 112772266,
            'lugar_expedicion' => 'Cartago valle',
            'fecha_nacimiento' => new \Carbon\Carbon('1990-12-01'),
            'civil_status_id' => 1,
            'direcion' => 'Calle 39d#1b 09',
            'barrio' => 'El danubio',
            'house_type_id' => 3,
            'telefono' => 3207301688,
            'correo' => 'yanethcorrea4020@gmail.com',
            'empleado' => false,
            'salario' => 414100,
            'fecha_expedicion' => new \Carbon\Carbon('2009-02-09'),
            'actividad_independiente' => 'Mesera',
            'vendedor' => 'Porfa yaneth correa patiño',
            'estado' => 'Aprobado',
            'created_at' => new \Carbon\Carbon('2019-06-13 19:19:44'),
            'updated_at' => new \Carbon\Carbon('2019-06-13 20:45:55'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 49,
            'nombre' => 'Paula Yissel Vargas Rodas',
            'type_document_id' => 1,
            'documento' => 1112764525,
            'lugar_expedicion' => 'Cartago',
            'fecha_nacimiento' =>   new \Carbon\Carbon('1988-05-06'),
            'civil_status_id' => 4,
            'direcion' => 'Mz c casa 14',
            'barrio' => 'El diamante',
            'house_type_id' => 1,
            'telefono' => 3153720413,
            'correo' => 'pyissel88@gmail.com',
            'empleado' => false,
            'salario' => 900000,
            'fecha_expedicion' =>  new \Carbon\Carbon('1988-07-11'),
            'actividad_independiente' => 'Independiente',
            'vendedor' => 'Ninguno',
            'estado' => 'Rechazado',
            'created_at' => new \Carbon\Carbon('2019-06-23 14:53:42'),
            'updated_at' => new \Carbon\Carbon('2019-06-23 15:27:45'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 41,
            'nombre' => 'Adriana del pilar Marulanda Uribe',
            'type_document_id' => 1,
            'documento' => 31410282,
            'lugar_expedicion' => 'Cartago',
            'fecha_nacimiento' =>   new \Carbon\Carbon('1966-03-17'),
            'civil_status_id' => 2,
            'direcion' => 'Carrera 5 a norte # 16b-08',
            'barrio' => 'Prado norte',
            'house_type_id' => 2,
            'telefono' => 3116309409,
            'correo' => 'wernercharron@gmail.com',
            'empleado' => true,
            'empresa' => 'Independiente',
            'profesion' => 'Publicista',
            'telefono_trabajo' => 3116309404,
            'direcion_empresa' => 'Carrera 5a norte # 16b-08',
            'salario' => 400000,
            'cargo' => 'Independiente',
            'tiempo_servicio' => new \Carbon\Carbon('2019-06-05'),
            'fecha_expedicion' =>  new \Carbon\Carbon('1985-03-25'),
            'vendedor' => 'No aplica',
            'estado' => 'Rechazado',
            'created_at' => new \Carbon\Carbon('2019-06-05 17:04:47'),
            'updated_at' => new \Carbon\Carbon('2019-08-17 13:33:44'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 48,
            'nombre' => 'Leidy jhoana grisales Morales',
            'type_document_id' => 1,
            'documento' => 31435856,
            'lugar_expedicion' => 'Cartago valle',
            'fecha_nacimiento' =>   new \Carbon\Carbon('1984-12-19'),
            'civil_status_id' => 2,
            'direcion' => 'Carrera 3 norte',
            'barrio' => 'San Carlos',
            'house_type_id' => 2,
            'telefono' => 3126194072,
            'correo' => 'Leidyjhoanagrisales@outlook.com',
            'empleado' => false,
            'salario' => 950,
            'fecha_expedicion' =>  new \Carbon\Carbon('2003-02-26'),
            'actividad_independiente' => 'Guarda seguridad',
            'procedencia' => 'Ninguno',
            'vendedor' => 'Almacen el punto',
            'estado' => 'Rechazado',
            'created_at' => new \Carbon\Carbon('2019-06-24 22:38:48'),
            'updated_at' => new \Carbon\Carbon('2019-06-25 19:27:03'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 50,
            'nombre' => 'Natalia Rocha',
            'type_document_id' => 1,
            'documento' => 1112759403,
            'lugar_expedicion' => 'Cartago',
            'fecha_nacimiento' =>   new \Carbon\Carbon('1986-05-27'),
            'civil_status_id' => 4,
            'direcion' => 'Carrera 8 a norte #19-51',
            'barrio' => 'Monte Verde',
            'house_type_id' => 3,
            'telefono' => 3135480967,
            'correo' => 'nrocha2717@gmail.com',
            'empleado' => true,
            'empresa' => 'Hospital San Juan de Dios',
            'profesion' => 'Enfermera Profesional',
            'telefono_trabajo' => 3104965438,
            'direcion_empresa' => ' Cra 3A con calle 2',
            'salario' => 2400000,
            'cargo' => 'Enfermera asistencial',
            'tiempo_servicio' => new \Carbon\Carbon('2016-10-20'),
            'fecha_expedicion' =>  new \Carbon\Carbon('2004-08-17'),
            'vendedor' => 'No lo sé',
            'estado' => 'Aprobado',
            'created_at' => new \Carbon\Carbon('2016-06-24 00:00:00'),
            'updated_at' => new \Carbon\Carbon('2016-06-25 00:00:00'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 51,
            'nombre' => 'FRANCIA TRINIDAD POSADA GIRALDO',
            'type_document_id' => 1,
            'documento' => 31433903,
            'lugar_expedicion' => 'cartago',
            'fecha_nacimiento' =>   new \Carbon\Carbon('1983-08-14'),
            'civil_status_id' => 1,
            'direcion' => 'CRA 5 5-22',
            'barrio' => 'GUADALUPE',
            'house_type_id' => 3,
            'telefono' => 3217491244,
            'correo' => 'franciatpg@hotmail.com',
            'empleado' => true,
            'empresa' => 'Lex Digital Group SAS',
            'profesion' => 'Tecnologa en sistemas',
            'telefono_trabajo' => 3165443869,
            'direcion_empresa' => 'Carrera 3 17-70',
            'salario' => 828116,
            'cargo' => 'Asistente Juridico',
            'tiempo_servicio' => new \Carbon\Carbon('2018-08-16'),
            'fecha_expedicion' =>  new \Carbon\Carbon('2001-10-12'),
            'vendedor' => 'N/A',
            'estado' => 'Aprobado',
            'created_at' => new \Carbon\Carbon('2019-06-27 22:13:58'),
            'updated_at' => new \Carbon\Carbon('2019-06-27 22:35:36'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 53,
            'nombre' => 'Jose Luis González Ciro',
            'type_document_id' => 1,
            'documento' => 1006318224,
            'lugar_expedicion' => 'Cartago',
            'fecha_nacimiento' =>   new \Carbon\Carbon('1997-09-28'),
            'civil_status_id' => 1,
            'direcion' => 'Calle 15 # 8-56',
            'barrio' => 'La Libertad',
            'house_type_id' => 3,
            'telefono' => 3165305948,
            'correo' => 'Joseciro1377@gmail.com',
            'empleado' => true,
            'empresa' => ' Frixio hecho a mano',
            'profesion' => 'Tecnologo',
            'telefono_trabajo' => 3137402856,
            'direcion_empresa' => 'Carrera 4 # 1A - 72',
            'salario' => 828116,
            'cargo' => 'Desarrollador',
            'tiempo_servicio' => new \Carbon\Carbon('2019-06-13'),
            'fecha_expedicion' =>  new \Carbon\Carbon('2015-10-09'),
            'vendedor' => 'Carlos Andrés Ronderos',
            'estado' => 'Aprobado',
            'created_at' => new \Carbon\Carbon('2016-06-27 00:00:00'),
            'updated_at' => new \Carbon\Carbon('2016-06-28 00:00:00'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 55,
            'nombre' => 'Maryuri Rios Villa',
            'type_document_id' => 1,
            'documento' => 1114088781,
            'lugar_expedicion' => 'Ansermanuevo',
            'fecha_nacimiento' =>   new \Carbon\Carbon('1986-09-09'),
            'civil_status_id' => 4,
            'direcion' => 'Diagonal 12 # 19-65',
            'barrio' => 'Torre La vega',
            'house_type_id' => 2,
            'telefono' => 3102245879,
            'correo' => 'maryurita198609@gmail.com',
            'empleado' => false,
            'salario' => 1100000,
            'fecha_expedicion' =>  new \Carbon\Carbon('2004-09-30'),
            'actividad_independiente' => 'Mercaderista',
            'otros_ingresos' => 300000,
            'procedencia' => 'Vendedora',
            'vendedor' => 'Maryuri Rios Villa',
            'estado' => 'Rechazado',
            'created_at' => new \Carbon\Carbon('2019-07-15 18:04:33'),
            'updated_at' => new \Carbon\Carbon('2019-07-15 19:36:42'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 56,
            'nombre' => 'Paula Andrea Meza Garcia',
            'type_document_id' => 1,
            'documento' => 1087990746,
            'lugar_expedicion' => 'Dos quebradas',
            'fecha_nacimiento' =>   new \Carbon\Carbon('1986-04-20'),
            'civil_status_id' => 1,
            'direcion' => 'Manzana 5 casa 54',
            'barrio' => 'Villa Elisa cuba',
            'house_type_id' => 2,
            'telefono' => 3138552618,
            'correo' => 'tmeza93@gmail.com',
            'empleado' => true,
            'empresa' => 'Gep col grupo empresarial punto',
            'profesion' => 'Auxiliar en engerneria',
            'telefono_trabajo' => 3117276730,
            'direcion_empresa' => 'Calle 11 número 7_74',
            'salario' => 1002810,
            'cargo' => 'Acesora comercial',
            'tiempo_servicio' => new \Carbon\Carbon('2019-02-19'),
            'fecha_expedicion' =>  new \Carbon\Carbon('2005-09-15'),
            'vendedor' => 'Ninguno',
            'estado' => 'Rechazado',
            'created_at' => new \Carbon\Carbon('2019-07-18 22:55:41'),
            'updated_at' => new \Carbon\Carbon('2019-07-18 23:58:34'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 54,
            'nombre' => 'Jessica Lorena Serna Del rio',
            'type_document_id' => 1,
            'documento' => 1112778822,
            'lugar_expedicion' => 'Cartago',
            'fecha_nacimiento' =>   new \Carbon\Carbon('1993-04-18'),
            'civil_status_id' => 4,
            'direcion' => 'CL 22 5 n 22',
            'barrio' => 'Guayacanes',
            'house_type_id' => 2,
            'telefono' => 3142029350,
            'correo' => 'jessica-serna@hotmail.com',
            'empleado' => false,
            'salario' => 1400000,
            'fecha_expedicion' =>  new \Carbon\Carbon('2011-05-04'),
            'actividad_independiente' => 'Jefe de sistemas',
            'vendedor' => 'Jessica serna',
            'estado' => 'Rechazado',
            'created_at' => new \Carbon\Carbon('2019-07-07 15:50:47'),
            'updated_at' => new \Carbon\Carbon('2019-07-22 13:30:19'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 57,
            'nombre' => '',
            'type_document_id' => 1,
            'documento' => 1006316413,
            'lugar_expedicion' => 'Ansermanuevo',
            'fecha_nacimiento' =>   new \Carbon\Carbon('1997-05-18'),
            'civil_status_id' => 7,
            'direcion' => 'Calle 13 a # 6-32',
            'barrio' => 'Vivienda obrera',
            'house_type_id' => 1,
            'telefono' => 3182014063,
            'correo' => 'tatianaospina810@hotmail.com',
            'empleado' => false,
            'salario' => 1000000,
            'fecha_expedicion' =>  new \Carbon\Carbon('2015-06-19'),
            'actividad_independiente' => 'Buena',
            'vendedor' => 'Tatiana ospina',
            'estado' => 'Rechazado',
            'created_at' => new \Carbon\Carbon('2019-06-27 22:35:29'),
            'updated_at' => new \Carbon\Carbon('2019-08-17 17:01:58'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 60,
            'nombre' => 'Xiomara Gómez Díaz',
            'type_document_id' => 1,
            'documento' => 1112762992,
            'lugar_expedicion' => 'Cartago valle',
            'fecha_nacimiento' =>  new \Carbon\Carbon('1987-10-15'),
            'civil_status_id' => 1,
            'direcion' => 'Carrera 1 # 37 106',
            'barrio' => 'Campoalegre',
            'house_type_id' => 3,
            'telefono' => 3116676436,
            'correo' => 'xiomaraboos5@gmail.com',
            'empleado' => false,
            'salario' => 1200000,
            'fecha_expedicion' =>  new \Carbon\Carbon('2005-11-23'),
            'actividad_independiente' => 'Empleada',
            'otros_ingresos' => 400000,
            'procedencia' => 'Hermanos en España',
            'vendedor' => 'Xiomara gomez',
            'estado' => 'Rechazado',
            'created_at' => new \Carbon\Carbon('2019-08-17 02:50:06'),
            'updated_at' => new \Carbon\Carbon('2019-08-17 13:28:39'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 63,
            'nombre' => 'Valentina castañeda walker',
            'type_document_id' => 1,
            'documento' => 1112794198,
            'lugar_expedicion' => 'Cartago',
            'fecha_nacimiento' =>   new \Carbon\Carbon('1999-10-06'),
            'civil_status_id' => 1,
            'direcion' => 'Calle 30 #2n-03 Mz19 C3',
            'barrio' => 'Cambulos',
            'house_type_id' => 1,
            'telefono' => 30089260800,
            'correo' => 'walkervalentina081@gmail.com',
            'empleado' => true,
            'empresa' => 'Orthomia',
            'profesion' => 'Auxiliar de salud oral',
            'telefono_trabajo' => 2094989,
            'direcion_empresa' => 'Calle 7#4-03',
            'salario' => 850,
            'cargo' => 'Auxiliar',
            'tiempo_servicio' => new \Carbon\Carbon('2019-02-01'),
            'fecha_expedicion' =>  new \Carbon\Carbon('2017-10-09'),
            'vendedor' => 'Valentina castañeda walker',
            'estado' => 'Aprobado',
            'created_at' => new \Carbon\Carbon('2019-08-18 23:08:56'),
            'updated_at' => new \Carbon\Carbon('2019-08-19 15:05:34'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 64,
            'nombre' => 'Viviana andrea quiroz utima',
            'type_document_id' => 1,
            'documento' => 31433615,
            'lugar_expedicion' => 'Cartago',
            'fecha_nacimiento' =>   new \Carbon\Carbon('1983-07-09'),
            'civil_status_id' => 2,
            'direcion' => 'Cra 13a #3-21',
            'barrio' => 'Berlin',
            'house_type_id' => 1,
            'telefono' => 3194998062,
            'correo' => 'andreaquiroz1983@hotmail.com',
            'empleado' => true,
            'empresa' => 'PENSIONADA',
            'salario' => 750,
            'cargo' => 'PENSIONADA',
            'fecha_expedicion' =>  new \Carbon\Carbon('2001-08-09'),
            'actividad_independiente' => 'Pensionada',
            'procedencia' => 'Arrendó	Viviana',
            'vendedor' => 'Viviana',
            'estado' => 'Aprobado',
            'created_at' => new \Carbon\Carbon('2019-08-28 19:01:55'),
            'updated_at' => new \Carbon\Carbon('2019-08-28 19:42:09'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 46,
            'nombre' => 'Andrés Felipe Vélez duque',
            'type_document_id' => 1,
            'documento' => 1112759458,
            'lugar_expedicion' => 'Cartago valle',
            'fecha_nacimiento' =>   new \Carbon\Carbon('2019-08-23'),
            'civil_status_id' => 1,
            'direcion' => 'C4 #28/51',
            'barrio' => 'Puerto Caldas',
            'house_type_id' => 3,
            'telefono' => 3137357910,
            'correo' => 'andres.f.velez85@gmail.com',
            'empleado' => true,
            'empresa' => 'Tempoeficas',
            'profesion' => 'Oficios varios',
            'telefono_trabajo' => 3346006,
            'direcion_empresa' => 'Calle 24 #7-29 Pereira',
            'salario' => 1000000,
            'cargo' => 'Oficios varios',
            'tiempo_servicio' => new \Carbon\Carbon('2018-10-15'),
            'fecha_expedicion' =>  new \Carbon\Carbon('1985-12-26'),
            'vendedor' => 'Andrea Naranjo',
            'estado' => 'Aprobado',
            'created_at' => new \Carbon\Carbon('2019-06-11 17:43:01'),
            'updated_at' => new \Carbon\Carbon('2019-06-11 19:40:43'),
        ]);
        DB::table('solicituds')->insert([
            'id' => 68,
            'nombre' => 'Sebastian Cano Cadavid',
            'type_document_id' => 1,
            'documento' => 1112788893,
            'lugar_expedicion' => 'Cartago Valle',
            'fecha_nacimiento' =>   new \Carbon\Carbon('1996-12-19'),
            'civil_status_id' => 1,
            'direcion' => 'Calle 4 #27-33',
            'barrio' => 'El sipres',
            'house_type_id' => 3,
            'telefono' => 2092612,
            'correo' => 'sebascano65@gmail.com',
            'empleado' => true,
            'empresa' => 'Frixio Casa de Bordado',
            'profesion' => 'Desarrollador de software',
            'telefono_trabajo' => 3146500,
            'direcion_empresa' => 'Calle 4 1A-72',
            'salario' => 828116,
            'cargo' => 'Web master',
            'tiempo_servicio' => new \Carbon\Carbon('2018-10-18'),
            'fecha_expedicion' =>  new \Carbon\Carbon('2015-01-13'),
            'vendedor' => 'Ninguno',
            'estado' => 'Aprobado',
            'created_at' => new \Carbon\Carbon('2019-08-29 00:00:00'),
            'updated_at' => new \Carbon\Carbon('2019-08-30 00:00:00'),
        ]);
        //spouses
        DB::table('spouses')->insert([
            'id' => 17,
            'nombre_conyuge' => 'Paula andrea',
            'numero_documento_conyuge' => 38460657,
            'trabajo' => 'Ama de casa',
            'telefono' => 3226093946,
            'salario' => 200000,
            'clase_actividad' => 'Ventas por catalogo',
            'request_id' => 40,
            'created_at' => new \Carbon\Carbon('2019-06-04 19:21:29'),
            'updated_at' => new \Carbon\Carbon('2019-06-04 19:21:29'),
        ]);
        DB::table('spouses')->insert([
            'id' => 18,
            'nombre_conyuge' => 'Werner Guillermo Freitag',
            'numero_documento_conyuge' => 16219092,
            'trabajo' => 'Independiente',
            'telefono' => 3173770047,
            'salario' => 5000000,
            'clase_actividad' => 'Odontologo',
            'request_id' => 41,
            'created_at' => new \Carbon\Carbon('2019-06-05 17:04:47'),
            'updated_at' => new \Carbon\Carbon('2019-06-05 17:04:47'),
        ]);
        DB::table('spouses')->insert([
            'id' => 20,
            'nombre_conyuge' => 'Jhon fredy cardona',
            'numero_documento_conyuge' => 6361879,
            'trabajo' => 'Independiente',
            'telefono' => 3147703320,
            'salario' => 1500000,
            'clase_actividad' => 'Conductor',
            'request_id' => 43,
            'created_at' => new \Carbon\Carbon('2019-06-05 22:45:00'),
            'updated_at' => new \Carbon\Carbon('2019-06-05 22:45:00'),
        ]);
        DB::table('spouses')->insert([
            'id' => 22,
            'nombre_conyuge' => 'Guillermo Antonio tobon',
            'numero_documento_conyuge' => 10199147,
            'trabajo' => 'Oficial construcción',
            'telefono' => 3106378943,
            'salario' => 850,
            'clase_actividad' => 'Oficial construcción',
            'request_id' => 48,
            'created_at' => new \Carbon\Carbon('2019-06-17 07:44:33'),
            'updated_at' => new \Carbon\Carbon('2019-06-17 07:44:33'),
        ]);
        DB::table('spouses')->insert([
            'id' => 23,
            'nombre_conyuge' => 'Andrés Calderón',
            'numero_documento_conyuge' => 93021448,
            'trabajo' => 'Bogota',
            'telefono' => 3215315834,
            'salario' => 2000000,
            'clase_actividad' => 'Empleado',
            'request_id' => 49,
            'created_at' => new \Carbon\Carbon('2019-06-23 14:53:42'),
            'updated_at' => new \Carbon\Carbon('2019-06-23 14:53:42'),
        ]);
        DB::table('spouses')->insert([
            'id' => 24,
            'nombre_conyuge' => 'Pablo Arturo Garcia Córdoba',
            'numero_documento_conyuge' => 14572574,
            'trabajo' => 'Indeboendiente',
            'telefono' => 3207206811,
            'salario' => 2000000,
            'clase_actividad' => 'Zootecnista',
            'request_id' => 50,
            'created_at' => new \Carbon\Carbon('2019-06-24 22:38:48'),
            'updated_at' => new \Carbon\Carbon('2019-06-24 22:38:48'),
        ]);
        DB::table('spouses')->insert([
            'id' => 25,
            'nombre_conyuge' => 'Valeria Sánchez Marín',
            'numero_documento_conyuge' => 1088036686,
            'trabajo' => 'Ingetronik ingeniería electrónica sas',
            'telefono' => 3127369628,
            'salario' => 12500000,
            'clase_actividad' => 'Jefe de compras',
            'request_id' => 54,
            'created_at' => new \Carbon\Carbon('2019-07-07 15:50:47'),
            'updated_at' => new \Carbon\Carbon('2019-07-07 15:50:47'),
        ]);
        DB::table('spouses')->insert([
            'id' => 26,
            'nombre_conyuge' => 'Jhoan stiven galvis',
            'numero_documento_conyuge' => 1114088781,
            'trabajo' => 'Securita',
            'telefono' => 3148800761,
            'salario' => 1200000,
            'clase_actividad' => 'Seguridad privada',
            'request_id' => 55,
            'created_at' => new \Carbon\Carbon('2019-07-15 18:04:33'),
            'updated_at' => new \Carbon\Carbon('2019-07-15 18:04:33'),
        ]);
        DB::table('spouses')->insert([
            'id' => 27,
            'nombre_conyuge' => 'Jamid Alejandro SERNA',
            'numero_documento_conyuge' => 1114091145,
            'trabajo' => 'Independiente',
            'telefono' => 3204088181,
            'salario' => 1500000,
            'clase_actividad' => 'Piloto de parapente',
            'request_id' => 57,
            'created_at' => new \Carbon\Carbon('2019-07-25 14:14:05'),
            'updated_at' => new \Carbon\Carbon('2019-07-25 14:14:05'),
        ]);
        DB::table('spouses')->insert([
            'id' => 28,
            'nombre_conyuge' => 'Jhon fredy torres restrepo',
            'numero_documento_conyuge' => 9958586,
            'trabajo' => 'Empresas municipales',
            'telefono' => 3155619405,
            'salario' => 850000,
            'clase_actividad' => 'Contratista',
            'request_id' => 64,
            'created_at' => new \Carbon\Carbon('2019-08-28 19:01:55'),
            'updated_at' => new \Carbon\Carbon('2019-08-28 19:01:55'),
        ]);
        //reference request
        DB::table('reference_request')->insert([
        'id' => 79,
        'request_id' => 40,
        'reference_id' => 79
    ]);
        DB::table('reference_request')->insert([
            'id' => 80,
            'request_id' => 40,
            'reference_id' => 80
        ]);
        DB::table('reference_request')->insert([
            'id' => 81,
            'request_id' => 41,
            'reference_id' => 81
        ]);
        DB::table('reference_request')->insert([
            'id' => 82,
            'request_id' => 41,
            'reference_id' => 82
        ]);
        DB::table('reference_request')->insert([
            'id' => 85,
            'request_id' => 43,
            'reference_id' => 85
        ]);
        DB::table('reference_request')->insert([
            'id' => 86,
            'request_id' => 43,
            'reference_id' => 86
        ]);
        DB::table('reference_request')->insert([
            'id' => 89,
            'request_id' => 45,
            'reference_id' => 89
        ]);
        DB::table('reference_request')->insert([
            'id' => 90,
            'request_id' => 45,
            'reference_id' => 90
        ]);
        DB::table('reference_request')->insert([
            'id' => 91,
            'request_id' => 46,
            'reference_id' => 91
        ]);
        DB::table('reference_request')->insert([
            'id' => 91,
            'request_id' => 46,
            'reference_id' => 91
        ]);
        DB::table('reference_request')->insert([
            'id' => 92,
            'request_id' => 46,
            'reference_id' => 92
        ]);
        DB::table('reference_request')->insert([
            'id' => 93,
            'request_id' => 47,
            'reference_id' => 93
        ]);
        DB::table('reference_request')->insert([
            'id' => 94,
            'request_id' => 47,
            'reference_id' => 94
        ]);
        DB::table('reference_request')->insert([
            'id' => 95,
            'request_id' => 48,
            'reference_id' => 95
        ]);
        DB::table('reference_request')->insert([
            'id' => 96,
            'request_id' => 48,
            'reference_id' => 96
        ]);
        DB::table('reference_request')->insert([
            'id' => 97,
            'request_id' => 49,
            'reference_id' => 97
        ]);
        DB::table('reference_request')->insert([
            'id' => 98,
            'request_id' => 49,
            'reference_id' => 98
        ]);
        DB::table('reference_request')->insert([
            'id' => 99,
            'request_id' => 50,
            'reference_id' => 99
        ]);
        DB::table('reference_request')->insert([
            'id' => 100,
            'request_id' => 50,
            'reference_id' => 100
        ]);
        DB::table('reference_request')->insert([
            'id' => 101,
            'request_id' => 51,
            'reference_id' => 101
        ]);
        DB::table('reference_request')->insert([
            'id' => 102,
            'request_id' => 51,
            'reference_id' => 102
        ]);
        DB::table('reference_request')->insert([
            'id' => 105,
            'request_id' => 53,
            'reference_id' => 105
        ]);
        DB::table('reference_request')->insert([
            'id' => 106,
            'request_id' => 53,
            'reference_id' => 106
        ]);
        DB::table('reference_request')->insert([
            'id' => 107,
            'request_id' => 54,
            'reference_id' => 107
        ]);
        DB::table('reference_request')->insert([
            'id' => 108,
            'request_id' => 54,
            'reference_id' => 108
        ]);
        DB::table('reference_request')->insert([
            'id' => 109,
            'request_id' => 55,
            'reference_id' => 109
        ]);
        DB::table('reference_request')->insert([
            'id' => 110,
            'request_id' => 55,
            'reference_id' => 110
        ]);
        DB::table('reference_request')->insert([
            'id' => 111,
            'request_id' => 56,
            'reference_id' => 111
        ]);
        DB::table('reference_request')->insert([
            'id' => 113,
            'request_id' => 57,
            'reference_id' => 113
        ]);
        DB::table('reference_request')->insert([
            'id' => 114,
            'request_id' => 57,
            'reference_id' => 114
        ]);
        DB::table('reference_request')->insert([
            'id' => 119,
            'request_id' => 60,
            'reference_id' => 119
        ]);
        DB::table('reference_request')->insert([
            'id' => 120,
            'request_id' => 60,
            'reference_id' => 120
        ]);
        DB::table('reference_request')->insert([
            'id' => 125,
            'request_id' => 63,
            'reference_id' => 125
        ]);
        DB::table('reference_request')->insert([
            'id' => 126,
            'request_id' => 63,
            'reference_id' => 126
        ]);
        DB::table('reference_request')->insert([
            'id' => 127,
            'request_id' => 64,
            'reference_id' => 127
        ]);
        DB::table('reference_request')->insert([
            'id' => 128,
            'request_id' => 64,
            'reference_id' => 128

        ]);
        DB::table('reference_request')->insert([
            'id' => 129,
            'request_id' => 65,
            'reference_id' => 129

        ]);
        DB::table('reference_request')->insert([
            'id' => 130,
            'request_id' => 66,
            'reference_id' => 130

        ]);
        // references
        DB::table('references')->insert([
            'id' => 1,
            'nombre' => 'saul antonio ronderos cortes',
            'direccion' => 'carrera 8 a norte #18-21',
            'telefono' => 3104126804,
            'ciudad' => 'cartago',
            'created_at' => new \Carbon\Carbon('2019-04-15 20:14:03'),
            'updated_at' => new \Carbon\Carbon('2019-04-15 20:14:03')
        ]);
        DB::table('references')->insert([
            'id' => 2,
            'nombre' => 'Maria fabiola opsina',
            'direccion' => 'carrera 8 a norte #18-21',
            'telefono' => 3124770278,
            'ciudad' => 'cartago',
            'created_at' => new \Carbon\Carbon('2019-04-15 20:14:03'),
            'updated_at' => new \Carbon\Carbon('2019-04-15 20:14:03')
        ]);
        DB::table('references')->insert([
            'id' => 3,
            'nombre' => 'saul antonio ronderos cortes',
            'direccion' => 'carrera 8 a norte #18-21',
            'telefono' => 3104126804,
            'ciudad' => 'cartago',
            'created_at' => new \Carbon\Carbon('2019-04-16 19:32:35'),
            'updated_at' => new \Carbon\Carbon('2019-04-16 19:32:35')
        ]);
        DB::table('references')->insert([
            'id' => 4,
            'nombre' => 'Maria fabiola opsina',
            'direccion' => 'carrera 8 a norte #18-21',
            'telefono' => 3124770278,
            'ciudad' => 'cartago',
            'created_at' => new \Carbon\Carbon('2019-04-16 19:32:35'),
            'updated_at' => new \Carbon\Carbon('2019-04-16 19:32:35')
        ]);
        DB::table('references')->insert([
            'id' => 5,
            'nombre' => 'saul antonio ronderos cortes',
            'direccion' => 'carrera 8 a norte #18-21',
            'telefono' => 3104126804,
            'ciudad' => 'cartago',
            'created_at' => new \Carbon\Carbon('2019-04-16 19:32:38'),
            'updated_at' => new \Carbon\Carbon('2019-04-16 19:32:38')
        ]);
        DB::table('references')->insert([
            'id' => 6,
            'nombre' => 'Maria fabiola opsina',
            'direccion' => 'carrera 8 a norte #18-21',
            'telefono' => 3124770278,
            'ciudad' => 'cartago',
            'created_at' => new \Carbon\Carbon('2019-04-16 19:32:38'),
            'updated_at' => new \Carbon\Carbon('2019-04-16 19:32:38')
        ]);
        DB::table('references')->insert([
            'id' => 7,
            'nombre' => 'Andres velez',
            'direccion' => 'Finca el diamante',
            'telefono' => 3137357910,
            'ciudad' => 'Armenia',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:31:12'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:31:12')
        ]);
        DB::table('references')->insert([
            'id' => 8,
            'nombre' => 'Ines florez',
            'direccion' => 'Puerto caldas',
            'telefono' => 3217218329,
            'ciudad' => 'Pereira',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:31:12'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:31:12')
        ]);
        DB::table('references')->insert([
            'id' => 9,
            'nombre' => 'Andres velez',
            'direccion' => 'Finca el diamante',
            'telefono' => 3137357910,
            'ciudad' => 'Armenia',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:31:16'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:31:16')
        ]);
        DB::table('references')->insert([
            'id' => 10,
            'nombre' => 'Ines florez',
            'direccion' => 'Puerto caldas',
            'telefono' => 3217218329,
            'ciudad' => 'Pereira',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:31:16'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:31:16')
        ]);
        DB::table('references')->insert([
            'id' => 11,
            'nombre' => 'Mariela perez',
            'direccion' => 'Calle 12 #10-29',
            'telefono' => 3125369863,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:35:00'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:35:00')
        ]);
        DB::table('references')->insert([
            'id' => 12,
            'nombre' => 'Leidy gomez',
            'direccion' => 'Calle 5 # 3 45',
            'telefono' => 3005232518,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:35:00'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:35:00')
        ]);
        DB::table('references')->insert([
            'id' => 13,
            'nombre' => 'Melba Valencia',
            'direccion' => 'Cra 4',
            'telefono' => 3152836969,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:35:08'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:35:08')
        ]);
        DB::table('references')->insert([
            'id' => 14,
            'nombre' => 'Alberto Olaya',
            'direccion' => 'Cra 4 # 28-69',
            'telefono' => 3206404313,
            'ciudad' => 'Pereira',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:35:08'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:35:08')
        ]);
        DB::table('references')->insert([
            'id' => 15,
            'nombre' => 'Alba Loaiza',
            'direccion' => 'CRA 5 #12-31',
            'telefono' => 3213845420,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:31'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:31')
        ]);
        DB::table('references')->insert([
            'id' => 16,
            'nombre' => 'Jorge loez',
            'direccion' => 'Calle 5#12_34',
            'telefono' => 3123153622,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:31'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:31')
        ]);
        DB::table('references')->insert([
            'id' => 17,
            'nombre' => 'Alba Loaiza',
            'direccion' => 'CRA 5 #12-31',
            'telefono' => 3213845420,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:35'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:35')
        ]);
        DB::table('references')->insert([
            'id' => 18,
            'nombre' => 'Jorge loez',
            'direccion' => 'Calle 5#12_34',
            'telefono' => 3123153622,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:35'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:35')
        ]);
        DB::table('references')->insert([
            'id' => 19,
            'nombre' => 'Alba Loaiza',
            'direccion' => 'CRA 5 #12-31',
            'telefono' => 3213845420,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:37'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:37')
        ]);
        DB::table('references')->insert([
            'id' => 20,
            'nombre' => 'Jorge loez',
            'direccion' => 'Calle 5#12_34',
            'telefono' => 3123153622,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:37'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:37')
        ]);
        DB::table('references')->insert([
            'id' => 21,
            'nombre' => 'Alba Loaiza',
            'direccion' => 'CRA 5 #12-31',
            'telefono' => 3213845420,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:38'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:38')
        ]);
        DB::table('references')->insert([
            'id' => 22,
            'nombre' => 'Jorge loez',
            'direccion' => 'Calle 5#12_34',
            'telefono' => 3123153622,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:38'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:38')
        ]);
        DB::table('references')->insert([
            'id' => 23,
            'nombre' => 'Alba Loaiza',
            'direccion' => 'CRA 5 #12-31',
            'telefono' => 3213845420,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:38'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:38')
        ]);
        DB::table('references')->insert([
            'id' => 24,
            'nombre' => 'Jorge loez',
            'direccion' => 'Calle 5#12_34',
            'telefono' => 3123153622,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:38'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:38')
        ]);
        DB::table('references')->insert([
            'id' => 25,
            'nombre' => 'Alba Loaiza',
            'direccion' => 'CRA 5 #12-31',
            'telefono' => 3213845420,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:39'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:39')
        ]);
        DB::table('references')->insert([
            'id' => 26,
            'nombre' => 'Jorge loez',
            'direccion' => 'Calle 5#12_34',
            'telefono' => 3123153622,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:39'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:39')
        ]);
        DB::table('references')->insert([
            'id' => 27,
            'nombre' => 'Consuelo Gomez',
            'direccion' => 'Cll 11 # 8-33',
            'telefono' => 3165832829,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:45'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:45')
        ]);
        DB::table('references')->insert([
            'id' => 28,
            'nombre' => 'Meri Florez',
            'direccion' => 'El vergel',
            'telefono' => 3218672009,
            'ciudad' => 'Ansermanuevo',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:45'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:45')
        ]);
        DB::table('references')->insert([
            'id' => 29,
            'nombre' => 'Consuelo Gomez',
            'direccion' => 'Cll 11 # 8-33',
            'telefono' => 3165832829,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:48'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:48')
        ]);
        DB::table('references')->insert([
            'id' => 30,
            'nombre' => 'Meri Florez',
            'direccion' => 'El vergel',
            'telefono' => 3218672009,
            'ciudad' => 'Ansermanuevo',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:36:48'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:36:48')
        ]);
        DB::table('references')->insert([
            'id' => 31,
            'nombre' => 'Santiago cruz',
            'direccion' => 'Calle 11 #4356',
            'telefono' => 3126807245,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:37:08'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:37:08')
        ]);
        DB::table('references')->insert([
            'id' => 32,
            'nombre' => 'Lupe arias',
            'direccion' => 'Calle 5 # 146',
            'telefono' => 3113587623,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:37:08'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:37:08')
        ]);
        DB::table('references')->insert([
            'id' => 33,
            'nombre' => 'Ana Roselly',
            'direccion' => 'Cardona	Carrera 3A 2-27',
            'telefono' => 3116063270,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:39:34'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:39:34')
        ]);
        DB::table('references')->insert([
            'id' => 34,
            'nombre' => 'Hugo Valencia',
            'direccion' => 'Carrera 2  18-10',
            'telefono' => 3116063271,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:39:34'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:39:34')
        ]);
        DB::table('references')->insert([
            'id' => 35,
            'nombre' => 'Laura escobar',
            'direccion' => 'Trasv 4b # 16-02',
            'telefono' => 3103841750,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:40:11'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:40:11')
        ]);
        DB::table('references')->insert([
            'id' => 36,
            'nombre' => 'Laura escobar',
            'direccion' => 'Trasv 4b #16-02',
            'telefono' => 3225555555,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:40:11'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:40:11')
        ]);
        DB::table('references')->insert([
            'id' => 37,
            'nombre' => 'Laura escobar',
            'direccion' => 'Trasv 4b # 16-02',
            'telefono' => 3103841750,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:40:14'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:40:14')
        ]);
        DB::table('references')->insert([
            'id' => 38,
            'nombre' => 'Laura escobar',
            'direccion' => 'Trasv 4b #16-02',
            'telefono' => 3225555555,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:40:14'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:40:14')
        ]);
        DB::table('references')->insert([
            'id' => 39,
            'nombre' => 'Edilia rios',
            'direccion' => 'Trans 4 b',
            'telefono' => 3222330013,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:40:25'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:40:25')
        ]);
        DB::table('references')->insert([
            'id' => 40,
            'nombre' => 'Luis escobar',
            'direccion' => 'Tranquilo 5',
            'telefono' => 3206792666,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:40:25'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:40:25')
        ]);
        DB::table('references')->insert([
            'id' => 41,
            'nombre' => 'Edilia rios',
            'direccion' => 'Trans 4 b',
            'telefono' => 3222330013,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:40:27'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:40:27')
        ]);
        DB::table('references')->insert([
            'id' => 42,
            'nombre' => 'Luis escobar',
            'direccion' => 'Tranquilo 5',
            'telefono' => 3206792666,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:40:27'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:40:27')
        ]);
        DB::table('references')->insert([
            'id' => 43,
            'nombre' => 'Claudia Lorena',
            'direccion' => 'Calle 4 #28 93',
            'telefono' => 3137314470,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:40:33'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:40:33')
        ]);
        DB::table('references')->insert([
            'id' => 44,
            'nombre' => 'Yeni maria',
            'direccion' => 'Cr 3 a 2 27',
            'telefono' => 3173795266,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:40:33'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:40:33')
        ]);
        DB::table('references')->insert([
            'id' => 45,
            'nombre' => 'Claudia Lorena',
            'direccion' => 'Calle 4 #28 93',
            'telefono' => 3137314470,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:40:39'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:40:39')
        ]);
        DB::table('references')->insert([
            'id' => 46,
            'nombre' => 'Yeni maria',
            'direccion' => 'Cr 3 a 2 27',
            'telefono' => 3173795266,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:40:33'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:40:33')
        ]);
        DB::table('references')->insert([
            'id' => 47,
            'nombre' => 'Idaly rios cárdenas',
            'direccion' => 'Calle 5 # 6-15',
            'telefono' => 3105071631,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:41:44'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:41:44')
        ]);
        DB::table('references')->insert([
            'id' => 48,
            'nombre' => 'Viviana rios',
            'direccion' => 'Calle4 # 15-26',
            'telefono' => 3214511085,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:41:44'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:41:44')
        ]);
        DB::table('references')->insert([
            'id' => 49,
            'nombre' => 'Idaly rios cárdenas',
            'direccion' => 'Calle 5 # 6-15',
            'telefono' => 3105071631,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:41:47'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:41:47')
        ]);
        DB::table('references')->insert([
            'id' => 50,
            'nombre' => 'Viviana rios',
            'direccion' => 'Calle4 # 15-26',
            'telefono' => 3214511085,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:41:47'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:41:47')
        ]);
        DB::table('references')->insert([
            'id' => 51,
            'nombre' => 'Andres velez',
            'direccion' => 'Finca el rodeo',
            'telefono' => 3146307245,
            'ciudad' => 'Armenia',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:42:55'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:42:55')
        ]);
        DB::table('references')->insert([
            'id' => 52,
            'nombre' => 'Liliana bedoya',
            'direccion' => 'Villa elena',
            'telefono' => 3127534646,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:42:55'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:42:55')
        ]);
        DB::table('references')->insert([
            'id' => 53,
            'nombre' => 'Elmer toro',
            'direccion' => 'Calle 15 12',
            'telefono' => 3124153514,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:46:09'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:46:09')
        ]);
        DB::table('references')->insert([
            'id' => 54,
            'nombre' => 'Yolanda toro',
            'direccion' => 'Cr 12 15',
            'telefono' => 3122873461,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:46:09'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:46:09')
        ]);
        DB::table('references')->insert([
            'id' => 55,
            'nombre' => 'Elmer toro',
            'direccion' => 'Calle 15 12',
            'telefono' => 3124153514,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:46:13'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:46:13')
        ]);
        DB::table('references')->insert([
            'id' => 56,
            'nombre' => 'Yolanda toro',
            'direccion' => 'Cr 12 15',
            'telefono' => 3122873461,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:46:13'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:46:13')
        ]);
        DB::table('references')->insert([
            'id' => 57,
            'nombre' => 'Elmer toro',
            'direccion' => 'Calle 15 12',
            'telefono' => 3124153514,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:46:15'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:46:15')
        ]);
        DB::table('references')->insert([
            'id' => 58,
            'nombre' => 'Yolanda toro',
            'direccion' => 'Cr 12 15',
            'telefono' => 3122873461,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-04-17 13:46:15'),
            'updated_at' => new \Carbon\Carbon('2019-04-17 13:46:15')
        ]);
        DB::table('references')->insert([
            'id' => 59,
            'nombre' => 'Deicy viviana marin',
            'direccion' => 'Cra 15 # 7t 54',
            'telefono' => 3144600719,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-05-06 17:32:37'),
            'updated_at' => new \Carbon\Carbon('2019-05-06 17:32:37')
        ]);
        DB::table('references')->insert([
            'id' => 60,
            'nombre' => 'Kevin alejandro marin',
            'direccion' => 'Los angeles mza 5 casa #3',
            'telefono' => 3108566763,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-05-06 17:32:37'),
            'updated_at' => new \Carbon\Carbon('2019-05-06 17:32:37')
        ]);
        DB::table('references')->insert([
            'id' => 61,
            'nombre' => 'Marcela rodriguez',
            'direccion' => 'Carrera 5 #12-49',
            'telefono' => 2130035,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-05-06 21:56:24'),
            'updated_at' => new \Carbon\Carbon('2019-05-06 21:56:24')
        ]);
        DB::table('references')->insert([
            'id' => 62,
            'nombre' => 'Gloria ossa',
            'direccion' => 'Carrera 5 #12-49',
            'telefono' => 2130035,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-05-06 21:56:24'),
            'updated_at' => new \Carbon\Carbon('2019-05-06 21:56:24')
        ]);
        DB::table('references')->insert([
            'id' => 63,
            'nombre' => 'Marcela rodriguez',
            'direccion' => 'Carrera 5 #12-49',
            'telefono' => 2130035,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-05-06 21:56:27'),
            'updated_at' => new \Carbon\Carbon('2019-05-06 21:56:27')
        ]);
        DB::table('references')->insert([
            'id' => 64,
            'nombre' => 'Gloria ossa',
            'direccion' => 'Carrera 5 #12-49',
            'telefono' => 2130035,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-05-06 21:56:27'),
            'updated_at' => new \Carbon\Carbon('2019-05-06 21:56:27')
        ]);
        DB::table('references')->insert([
            'id' => 65,
            'nombre' => 'Jhomer pavas',
            'direccion' => 'Calle 1e # 3-21 alameda',
            'telefono' => 3205511821,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-05-06 23:44:14'),
            'updated_at' => new \Carbon\Carbon('2019-05-06 23:44:14')
        ]);
        DB::table('references')->insert([
            'id' => 66,
            'nombre' => 'Luz elena castrillon',
            'direccion' => 'Calle 22b # 3dn -10 la linda',
            'telefono' => 3206650640,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-05-06 23:44:14'),
            'updated_at' => new \Carbon\Carbon('2019-05-06 23:44:14')
        ]);
        DB::table('references')->insert([
            'id' => 67,
            'nombre' => 'Camilo Agudelo',
            'direccion' => 'mz 5 cs 13 la cabaña cartago',
            'telefono' => 3145896841,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-05-08 21:52:02'),
            'updated_at' => new \Carbon\Carbon('2019-05-08 21:52:02')
        ]);
        DB::table('references')->insert([
            'id' => 68,
            'nombre' => 'Camila Martinez Bueno',
            'direccion' => 'Carrera 11 # 11-71',
            'telefono' => 3022905701,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-05-08 21:52:02'),
            'updated_at' => new \Carbon\Carbon('2019-05-08 21:52:02')
        ]);
        DB::table('references')->insert([
            'id' => 69,
            'nombre' => 'Manuel enrique banguero',
            'direccion' => 'Palmira',
            'telefono' => 3234783704,
            'ciudad' => 'Palmira valle',
            'created_at' => new \Carbon\Carbon('2019-05-30 23:47:37'),
            'updated_at' => new \Carbon\Carbon('2019-05-30 23:47:37')
        ]);
        DB::table('references')->insert([
            'id' => 70,
            'nombre' => 'Olga liliana toro rios',
            'direccion' => 'Cra 10 # 17- 23',
            'telefono' => 3174651881,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-05-30 23:47:37'),
            'updated_at' => new \Carbon\Carbon('2019-05-30 23:47:37')
        ]);
        DB::table('references')->insert([
            'id' => 71,
            'nombre' => 'Manuel enrique banguero',
            'direccion' => 'Palmira',
            'telefono' => 3234783704,
            'ciudad' => 'Palmira valle',
            'created_at' => new \Carbon\Carbon('2019-05-30 23:47:43'),
            'updated_at' => new \Carbon\Carbon('2019-05-30 23:47:43')
        ]);
        DB::table('references')->insert([
            'id' => 72,
            'nombre' => 'Olga liliana toro rios',
            'direccion' => 'Cra 10 # 17- 23',
            'telefono' => 3174651881,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-05-30 23:47:43'),
            'updated_at' => new \Carbon\Carbon('2019-05-30 23:47:43')
        ]);
        DB::table('references')->insert([
            'id' => 73,
            'nombre' => 'Manuel enrique banguero',
            'direccion' => 'Palmira',
            'telefono' => 3234783704,
            'ciudad' => 'Palmira valle',
            'created_at' => new \Carbon\Carbon('2019-05-30 23:47:46'),
            'updated_at' => new \Carbon\Carbon('2019-05-30 23:47:46')
        ]);
        DB::table('references')->insert([
            'id' => 74,
            'nombre' => 'Olga liliana toro rios',
            'direccion' => 'Cra 10 # 17- 23',
            'telefono' => 3174651881,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-05-30 23:47:46'),
            'updated_at' => new \Carbon\Carbon('2019-05-30 23:47:46')
        ]);
        DB::table('references')->insert([
            'id' => 75,
            'nombre' => 'Manuel enrique banguero',
            'direccion' => 'Palmira',
            'telefono' => 3234783704,
            'ciudad' => 'Palmira valle',
            'created_at' => new \Carbon\Carbon('2019-05-30 23:47:50'),
            'updated_at' => new \Carbon\Carbon('2019-05-30 23:47:50')
        ]);
        DB::table('references')->insert([
            'id' => 76,
            'nombre' => 'Olga liliana toro rios',
            'direccion' => 'Cra 10 # 17- 23',
            'telefono' => 3174651881,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-05-30 23:47:50'),
            'updated_at' => new \Carbon\Carbon('2019-05-30 23:47:50')
        ]);
        DB::table('references')->insert([
            'id' => 77,
            'nombre' => 'César Vargas',
            'direccion' => 'Kra 2 # 34-56',
            'telefono' => 3114151946,
            'ciudad' => 'Cartgo',
            'created_at' => new \Carbon\Carbon('2019-06-04 19:21:28'),
            'updated_at' => new \Carbon\Carbon('2019-06-04 19:21:28')
        ]);
        DB::table('references')->insert([
            'id' => 78,
            'nombre' => '	Leiby cifuentes',
            'direccion' => 'Calle 78b #26g3 -57',
            'telefono' => 3122388576,
            'ciudad' => 'Cali',
            'created_at' => new \Carbon\Carbon('2019-06-04 19:21:28'),
            'updated_at' => new \Carbon\Carbon('2019-06-04 19:21:28')
        ]);
        DB::table('references')->insert([
            'id' => 80,
            'nombre' => 'Leiby cifuentes',
            'direccion' => 'Calle 78b #26g3 -57',
            'telefono' => 3122388576,
            'ciudad' => 'Cali',
            'created_at' => new \Carbon\Carbon('2019-06-04 19:21:29'),
            'updated_at' => new \Carbon\Carbon('2019-06-04 19:21:29')
        ]);
        DB::table('references')->insert([
            'id' => 81,
            'nombre' => 'Werner Freitag Marulanda',
            'direccion' => 'Carrera 2da norte # 12-16',
            'telefono' => 3175695684,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-06-05 17:04:47'),
            'updated_at' => new \Carbon\Carbon('2019-08-17 13:33:44')
        ]);
        DB::table('references')->insert([
            'id' => 82,
            'nombre' => 'Juliana freitag',
            'direccion' => 'Carrera 5 a norte #16b - 08',
            'telefono' => 3128929539,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-06-05 17:04:47'),
            'updated_at' => new \Carbon\Carbon('2019-06-05 17:04:47')
        ]);
        DB::table('references')->insert([
            'id' => 83,
            'nombre' => 'Werner Freitag Marulanda',
            'direccion' => 'Carrera 2da norte # 12-16',
            'telefono' => 3175695684,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-06-05 17:04:50'),
            'updated_at' => new \Carbon\Carbon('2019-06-05 17:04:50')
        ]);
        DB::table('references')->insert([
            'id' => 84,
            'nombre' => '	Juliana freitag',
            'direccion' => 'Carrera 5 a norte #16b - 08',
            'telefono' => 3128929539,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-06-05 17:04:50'),
            'updated_at' => new \Carbon\Carbon('2019-06-05 17:04:50')
        ]);
        DB::table('references')->insert([
            'id' => 85,
            'nombre' => 'Amanda rubiano',
            'direccion' => 'Calle 12 oeste # 47-70',
            'telefono' => 3136242628,
            'ciudad' => 'Cali valle',
            'created_at' => new \Carbon\Carbon('2019-06-05 22:45:00'),
            'updated_at' => new \Carbon\Carbon('2019-06-05 22:45:00')
        ]);
        DB::table('references')->insert([
            'id' => 86,
            'nombre' => 'Elizabeth caro',
            'direccion' => 'Carrera 1 norte # 54-27',
            'telefono' => 3137512216,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-06-05 22:45:00'),
            'updated_at' => new \Carbon\Carbon('2019-06-05 22:45:00')
        ]);
        DB::table('references')->insert([
            'id' => 87,
            'nombre' => 'Amanda rubiano',
            'direccion' => 'Calle 12 oeste # 47-70',
            'telefono' => 3136242628,
            'ciudad' => 'Cali valle',
            'created_at' => new \Carbon\Carbon('2019-06-05 22:45:33'),
            'updated_at' => new \Carbon\Carbon('2019-06-05 22:45:33')
        ]);
        DB::table('references')->insert([
            'id' => 88,
            'nombre' => '	Elizabeth caro',
            'direccion' => 'Carrera 1 norte # 54-27',
            'telefono' => 3137512216,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-06-05 22:45:33'),
            'updated_at' => new \Carbon\Carbon('2019-06-05 22:45:33')
        ]);
        DB::table('references')->insert([
            'id' => 89,
            'nombre' => 'Libia Ines marulanda',
            'direccion' => 'Cra 5 Anorte 16b 15',
            'telefono' => 3122490387,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-06-07 16:01:46'),
            'updated_at' => new \Carbon\Carbon('2019-06-07 16:01:46')
        ]);
        DB::table('references')->insert([
            'id' => 90,
            'nombre' => 'María Elena Marulanda',
            'direccion' => 'Cra 2 norte 12-22',
            'telefono' => 3108200696,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-06-07 16:01:46'),
            'updated_at' => new \Carbon\Carbon('2019-06-07 16:01:46')
        ]);
        DB::table('references')->insert([
            'id' => 91,
            'nombre' => 'Víctor muños',
            'direccion' => 'C 4 # 23-52',
            'telefono' => 3108655651,
            'ciudad' => 'Puerto Caldas',
            'created_at' => new \Carbon\Carbon('2019-06-11 17:43:01'),
            'updated_at' => new \Carbon\Carbon('2019-06-11 17:43:01')
        ]);
        DB::table('references')->insert([
            'id' => 92,
            'nombre' => 'Sofía Ruiz',
            'direccion' => 'Cr4#28_06',
            'telefono' => 3147102601,
            'ciudad' => 'Pereira',
            'created_at' => new \Carbon\Carbon('2019-06-11 17:43:01'),
            'updated_at' => new \Carbon\Carbon('2019-06-11 17:43:01')
        ]);
        DB::table('references')->insert([
            'id' => 93,
            'nombre' => 'Yamis mosquera',
            'direccion' => 'San pablo',
            'telefono' => 3113422896,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-06-13 19:19:44'),
            'updated_at' => new \Carbon\Carbon('2019-06-13 19:19:44')
        ]);
        DB::table('references')->insert([
            'id' => 94,
            'nombre' => 'Sandra patricia osorio',
            'direccion' => 'Kirama',
            'telefono' => 3147138620,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-06-13 19:19:44'),
            'updated_at' => new \Carbon\Carbon('2019-06-13 19:19:44')
        ]);
        DB::table('references')->insert([
            'id' => 95,
            'nombre' => 'Diana Patricia Grisales',
            'direccion' => 'Carrera 3 norte 33-40',
            'telefono' => 3137349432,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-06-17 07:44:33'),
            'updated_at' => new \Carbon\Carbon('2019-08-17 13:34:05')
        ]);
        DB::table('references')->insert([
            'id' => 96,
            'nombre' => 'María fabiola morales',
            'direccion' => 'Carrera 1D 40-40',
            'telefono' => 3136896237,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-06-17 07:44:33'),
            'updated_at' => new \Carbon\Carbon('2019-06-17 07:44:33')
        ]);
        DB::table('references')->insert([
            'id' => 97,
            'nombre' => 'Jhon jairo Vargas',
            'direccion' => 'Calle 8 12 61',
            'telefono' => 3113931656,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-06-23 14:53:42'),
            'updated_at' => new \Carbon\Carbon('2019-06-23 14:53:42')
        ]);
        DB::table('references')->insert([
            'id' => 98,
            'nombre' => 'Gustavo Vargas',
            'direccion' => 'Trav 7 carrera 14 5 a 66',
            'telefono' => 3216314975,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-06-23 14:53:42'),
            'updated_at' => new \Carbon\Carbon('2019-06-23 14:53:42')
        ]);
        DB::table('references')->insert([
            'id' => 99,
            'nombre' => 'Maria Piedad Villanueva',
            'direccion' => 'Carrera 8 a norte # 19-59',
            'telefono' => 3177283762,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-06-24 22:38:48'),
            'updated_at' => new \Carbon\Carbon('2019-06-24 22:38:48')
        ]);
        DB::table('references')->insert([
            'id' => 100,
            'nombre' => 'Pablo García Córdoba',
            'direccion' => 'Cra 8 a norte # 19-51',
            'telefono' => 3207206811,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-06-24 22:38:48'),
            'updated_at' => new \Carbon\Carbon('2019-06-24 22:38:48')
        ]);

        DB::table('references')->insert([
            'id' => 101,
            'nombre' => 'EDUAR SAMUEL POSADA',
            'direccion' => 'CALLE 24A 3A-03 APTO 306',
            'telefono' => 3113979748,
            'ciudad' => 'CARTAGO',
            'created_at' => new \Carbon\Carbon('2019-06-27 22:13:58'),
            'updated_at' => new \Carbon\Carbon('2019-06-27 22:13:58')
        ]);
        DB::table('references')->insert([
            'id' => 102,
            'nombre' => 'ESMERALDA BUENO',
            'direccion' => 'CALLE 25D 3-21',
            'telefono' => 3217295181,
            'ciudad' => 'CARTAGO',
            'created_at' => new \Carbon\Carbon('2019-06-27 22:13:58'),
            'updated_at' => new \Carbon\Carbon('2019-06-27 22:13:58')
        ]);
        DB::table('references')->insert([
            'id' => 103,
            'nombre' => 'Madelayne ciro',
            'direccion' => 'Calle 15 # 8-56',
            'telefono' => 3152853588,
            'ciudad' => '31423413',
            'created_at' => new \Carbon\Carbon('2019-06-27 22:16:58'),
            'updated_at' => new \Carbon\Carbon('2019-06-27 22:16:58')
        ]);
        DB::table('references')->insert([
            'id' => 104,
            'nombre' => 'Jose Luis Gonzalez',
            'direccion' => 'Calle 15#8-56',
            'telefono' => 3136298659,
            'ciudad' => '16228279',
            'created_at' => new \Carbon\Carbon('2019-06-27 22:16:58'),
            'updated_at' => new \Carbon\Carbon('2019-06-27 22:16:58')
        ]);
        DB::table('references')->insert([
            'id' => 105,
            'nombre' => 'Madelayne Ciro montoya',
            'direccion' => 'Calle 15 # 8-56',
            'telefono' => 3152853588,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-06-27 22:35:29'),
            'updated_at' => new \Carbon\Carbon('2019-06-27 22:35:29')
        ]);
        DB::table('references')->insert([
            'id' => 106,
            'nombre' => 'Jose Luis Gonzalez',
            'direccion' => 'Calle 15 # 8_55',
            'telefono' => 3136298659,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-06-27 22:35:29'),
            'updated_at' => new \Carbon\Carbon('2019-06-27 22:35:29')
        ]);
        DB::table('references')->insert([
            'id' => 107,
            'nombre' => 'Norberto del rio',
            'direccion' => 'CL 20 a 5 n 29',
            'telefono' => 3136523149,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-07-07 15:50:47'),
            'updated_at' => new \Carbon\Carbon('2019-07-07 15:50:47')
        ]);
        DB::table('references')->insert([
            'id' => 108,
            'nombre' => 'Gladys serna',
            'direccion' => 'CL 20 a 5 n19',
            'telefono' => 2122174,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-07-07 15:50:47'),
            'updated_at' => new \Carbon\Carbon('2019-07-07 15:50:47')
        ]);
        DB::table('references')->insert([
            'id' => 109,
            'nombre' => 'Doris villa',
            'direccion' => 'Bogotá',
            'telefono' => 3124445055,
            'ciudad' => 'Bogotá',
            'created_at' => new \Carbon\Carbon('2019-07-15 18:04:33'),
            'updated_at' => new \Carbon\Carbon('2019-07-15 18:04:33')
        ]);
        DB::table('references')->insert([
            'id' => 110,
            'nombre' => 'Loren Omaira Velázquez',
            'direccion' => 'Ansermanuevo',
            'telefono' => 3117224460,
            'ciudad' => 'Ansermanuevo',
            'created_at' => new \Carbon\Carbon('2019-07-15 18:04:33'),
            'updated_at' => new \Carbon\Carbon('2019-07-15 18:04:33')
        ]);
        DB::table('references')->insert([
            'id' => 111,
            'nombre' => 'Jennifer oviedo maya',
            'direccion' => 'Carr 25 vis 72c50',
            'telefono' => 3142629713,
            'ciudad' => 'Pereira',
            'created_at' => new \Carbon\Carbon('2019-07-18 22:55:41'),
            'updated_at' => new \Carbon\Carbon('2019-07-18 22:55:41')
        ]);
        DB::table('references')->insert([
            'id' => 112,
            'nombre' => 'Carmen Tulia Mesa Garcia',
            'direccion' => 'Manzana 5 casa 54',
            'telefono' => 3137380270,
            'ciudad' => 'Pereira',
            'created_at' => new \Carbon\Carbon('2019-07-18 22:55:41'),
            'updated_at' => new \Carbon\Carbon('2019-07-18 22:55:41')
        ]);
        DB::table('references')->insert([
            'id' => 113,
            'nombre' => 'Luz Marina Giraldo',
            'direccion' => 'Clle98bcra83b # 29 interior 202 el picacho',
            'telefono' => 3223864121,
            'ciudad' => 'Medellín',
            'created_at' => new \Carbon\Carbon('2019-07-25 14:14:05'),
            'updated_at' => new \Carbon\Carbon('2019-07-25 14:14:05')
        ]);
        DB::table('references')->insert([
            'id' => 114,
            'nombre' => 'Maria osorio',
            'direccion' => '13 a # 6-53',
            'telefono' => 3128101276,
            'ciudad' => 'Ansermanuevo',
            'created_at' => new \Carbon\Carbon('2019-07-25 14:14:05'),
            'updated_at' => new \Carbon\Carbon('2019-07-25 14:14:05')
        ]);
        DB::table('references')->insert([
            'id' => 115,
            'nombre' => 'Takdbdvakd jakdjd',
            'direccion' => 'Haidvajdnfj',
            'telefono' => 61843784,
            'ciudad' => 'Gqidhshak',
            'created_at' => new \Carbon\Carbon('2019-08-16 15:52:34'),
            'updated_at' => new \Carbon\Carbon('2019-08-16 15:52:34')
        ]);
        DB::table('references')->insert([
            'id' => 116,
            'nombre' => 'Gaks jahdka',
            'direccion' => 'Hakdhskd',
            'telefono' => 6497648,
            'ciudad' => 'Gakdva',
            'created_at' => new \Carbon\Carbon('2019-08-16 15:52:34'),
            'updated_at' => new \Carbon\Carbon('2019-08-16 15:52:34')
        ]);
        DB::table('references')->insert([
            'id' => 117,
            'nombre' => 'nbcnsxczx czxnxcb',
            'direccion' => 'asdkgasjdghasd',
            'telefono' => 456456456456,
            'ciudad' => 'asdasdasdasdas',
            'created_at' => new \Carbon\Carbon('2019-08-16 22:41:45'),
            'updated_at' => new \Carbon\Carbon('2019-08-16 22:41:45')
        ]);
        DB::table('references')->insert([
            'id' => 118,
            'nombre' => 'sd sadas dasda',
            'direccion' => 'asdgashdashdhasd',
            'telefono' => 1321231235664,
            'ciudad' => 'sadjasdfhasdg',
            'created_at' => new \Carbon\Carbon('2019-08-16 22:41:45'),
            'updated_at' => new \Carbon\Carbon('2019-08-16 22:41:45')
        ]);
        DB::table('references')->insert([
            'id' => 119,
            'nombre' => 'Andrés rivera',
            'direccion' => 'Carre 3 # 33 -63',
            'telefono' => 3117205058,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-08-17 02:50:06'),
            'updated_at' => new \Carbon\Carbon('2019-08-17 02:50:06')
        ]);
        DB::table('references')->insert([
            'id' => 120,
            'nombre' => 'Pamela zapata',
            'direccion' => 'Carrera 1 # 37- 87',
            'telefono' => 3148199838,
            'ciudad' => 'Cartago valle',
            'created_at' => new \Carbon\Carbon('2019-08-17 02:50:06'),
            'updated_at' => new \Carbon\Carbon('2019-08-17 02:50:06')
        ]);
        DB::table('references')->insert([
            'id' => 121,
            'nombre' => 'Jdjdjjqhd sjjdb',
            'direccion' => 'Djjshdhskbd',
            'telefono' => 4946764,
            'ciudad' => 'Hshajvfbs',
            'created_at' => new \Carbon\Carbon('2019-08-17 13:54:52'),
            'updated_at' => new \Carbon\Carbon('2019-08-17 13:54:52')
        ]);
        DB::table('references')->insert([
            'id' => 122,
            'nombre' => 'Hsjahd ajjf',
            'direccion' => 'Jsjshhfjsj',
            'telefono' => 1649784,
            'ciudad' => 'Hsjsjbf',
            'created_at' => new \Carbon\Carbon('2019-08-17 13:54:52'),
            'updated_at' => new \Carbon\Carbon('2019-08-17 13:54:52')
        ]);
        DB::table('references')->insert([
            'id' => 123,
            'nombre' => 'Hfiwhfjwhf jajfjdjqjf',
            'direccion' => 'Gdakhdjajdjd',
            'telefono' => 619467955,
            'ciudad' => 'Gsjagdjshd',
            'created_at' => new \Carbon\Carbon('2019-08-17 17:07:32'),
            'updated_at' => new \Carbon\Carbon('2019-08-17 17:07:32')
        ]);
        DB::table('references')->insert([
            'id' => 124,
            'nombre' => 'Hdkahdj hjdkshd',
            'direccion' => 'Gsiahfjsjd',
            'telefono' => 616464945,
            'ciudad' => 'Gskagdja',
            'created_at' => new \Carbon\Carbon('2019-08-17 17:07:32'),
            'updated_at' => new \Carbon\Carbon('2019-08-17 17:07:32')
        ]);
        DB::table('references')->insert([
            'id' => 125,
            'nombre' => 'Luz Andrea Orrego',
            'direccion' => 'Calle 30 #2n-03 Mz19 C3',
            'telefono' => 2094112,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-08-18 23:08:56'),
            'updated_at' => new \Carbon\Carbon('2019-08-18 23:08:56')
        ]);
        DB::table('references')->insert([
            'id' => 126,
            'nombre' => 'Eduardo escobar',
            'direccion' => 'Calle 3 #65-101',
            'telefono' => 3104170186,
            'ciudad' => 'Cali',
            'created_at' => new \Carbon\Carbon('2019-08-18 23:08:56'),
            'updated_at' => new \Carbon\Carbon('2019-08-18 23:08:56')
        ]);
        DB::table('references')->insert([
            'id' => 127,
            'nombre' => 'Maria Gladis utima',
            'direccion' => 'San pablo',
            'telefono' => 3128484711,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-08-28 19:01:55'),
            'updated_at' => new \Carbon\Carbon('2019-08-28 19:42:09')
        ]);
        DB::table('references')->insert([
            'id' => 128,
            'nombre' => 'Luz dary utima',
            'direccion' => 'Calle 1 e# 1a29 ortez',
            'telefono' => 3146767628,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-08-28 19:01:55'),
            'updated_at' => new \Carbon\Carbon('2019-08-28 19:28:21')
        ]);
        DB::table('references')->insert([
            'id' => 129,
            'nombre' => 'Maria Gladis utima',
            'direccion' => 'San pablo',
            'telefono' => 3128484711,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-08-28 19:11:39'),
            'updated_at' => new \Carbon\Carbon('2019-08-28 19:11:39')
        ]);
        DB::table('references')->insert([
            'id' => 130,
            'nombre' => 'Luz dary utima',
            'direccion' => 'Calle 1e # 1a29 ortez',
            'telefono' => 3146767628,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-08-28 19:11:39'),
            'updated_at' => new \Carbon\Carbon('2019-08-28 19:11:39')
        ]);
        DB::table('references')->insert([
            'id' => 131,
            'nombre' => 'Jhan Carlos Lucio',
            'direccion' => 'Calle 11A 1N-17',
            'telefono' => 3014157245,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-08-28 19:54:30'),
            'updated_at' => new \Carbon\Carbon('2019-08-28 19:54:30')
        ]);
        DB::table('references')->insert([
            'id' => 132,
            'nombre' => 'Juan Camilo Agudelo',
            'direccion' => 'Manzana 5 casa 13',
            'telefono' => 3145896841,
            'ciudad' => 'Cartago',
            'created_at' => new \Carbon\Carbon('2019-08-28 19:54:30'),
            'updated_at' => new \Carbon\Carbon('2019-08-28 19:54:30')
        ]);

    }
}
