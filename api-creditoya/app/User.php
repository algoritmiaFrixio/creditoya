<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string usuario
 * @property string contrasena
 */
class User extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

}
