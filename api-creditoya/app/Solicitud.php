<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    protected $hidden = ['updated_at'];
    protected $fillable = ['nombre', 'type_document_id', 'documento', 'lugar_expedicion', 'civil_status_id'];

    public function civil_status()
    {
        return $this->belongsTo(CivilStatus::class, 'civil_status_id', 'id');
    }

    public function house_type()
    {
        return $this->belongsTo(HouseType::class, 'house_type_id', 'id');
    }

    public function spouse()
    {
        return $this->hasOne(Spouse::class, 'request_id', 'id');
    }

    public function type_document()
    {
        return $this->belongsTo(TypeDocument::class, 'type_document_id', 'id');
    }

    public function references()
    {
        return $this->belongsToMany(Reference::class, 'reference_request', 'request_id', 'reference_id', 'id', 'id');
    }
}
