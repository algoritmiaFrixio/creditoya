<?php

namespace App\Http\Controllers;

use App\tokens;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse as JsonResponseAlias;
use Illuminate\Http\Request;
use Throwable;

class TokensController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponseAlias
     */
    public function index(Request $request)
    {
        if ($request->isJson() && $request->has('token')) {
            return response()->json(tokens::all(), 200);
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @return JsonResponseAlias
     */
    public function store(Request $request)
    {
        if ($request->isJson() && $request->has('token')) {
            try {
                $count = tokens::where('id_user', $request->id_user)->count();
                if ($count === 0) {
                    $token = new tokens();
                } else {
                    $token = tokens::where('id_user', $request->id_user)->firstOrFail();
                }
                $token->token = $request->token;
                $token->id_user = $request->id_user;
                $token->saveOrFail();
                return response()->json(['message' => 'Notificaciones configuradas correctamente'], 200);
            } catch (Throwable $e) {
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\tokens $tokens
     * @return \Illuminate\Http\Response
     */
    public function show(tokens $tokens)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\tokens $tokens
     * @return \Illuminate\Http\Response
     */
    public function edit(tokens $tokens)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\tokens $tokens
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tokens $tokens)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\tokens $tokens
     * @return \Illuminate\Http\Response
     */
    public function destroy(tokens $tokens)
    {
        //
    }
}
