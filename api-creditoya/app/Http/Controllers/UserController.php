<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function auth(Request $request)
    {
        if ($request->isJson() && $request->has('token')) {
            try {

                $user = User::where('usuario', $request->usuario)->firstOrFail();
                if (Hash::check($request->contrasena, $user->contrasena)) {
                    return response()->json(['user' => $user, 'message' => 'Datos correctos'], 200);
                }
                return response()->json(['message' => 'Usuario y/o contraseña incorrecta'], 401);
            } catch (ModelNotFoundException $exception) {
                return response()->json(['message' => $exception->getMessage()], 401);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function addUsers()
    {
        $user = new User();
        $user->usuario = 'guillermo';
        $user->contrasena = Hash::make('14566195');
        if ($user->save()) {
            $user = new User();
            $user->usuario = 'widmar';
            $user->contrasena = Hash::make('1112787270');
            if ($user->save()) {
                $user = new User();
                $user->usuario = 'marcela';
                $user->contrasena = Hash::make('1112779703');
                if ($user->save()) {
                    $user = new User();
                    $user->usuario = 'adriana';
                    $user->contrasena = Hash::make('17537558');
                    if ($user->save()) {
                        $user = new User();
                        $user->usuario = 'diana';
                        $user->contrasena = Hash::make('31433904');
                        if ($user->save()) {
                            return response()->json(['todos agregados'], 200);
                        }
                        return response()->json(['error en usuario diana'], 500);
                    }
                    return response()->json(['error en usuario adriana'], 500);
                }
                return response()->json(['error en usuario marcela'], 500);
            }
            return response()->json(['error en usuario widmar'], 500);
        }
        return response()->json(['error en usuario guillermo'], 500);
    }
}
