<?php

namespace App\Http\Controllers;

use App\Reference;
use App\Solicitud;
use App\Spouse;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SolicitudController extends Controller
{

    public function inicio(Request $request)
    {
        if ($request->isJson()) {
            return response()->json(['token' => substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 60)], 200);
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /*
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->isJson() && $request->has('token')) {
            if ($request->ano !== "todos") {
                $requests = Solicitud::with(['civil_status', 'house_type', 'spouse', 'type_document', 'references'])
                    ->whereYear('created_at', $request->ano)
                    ->get();
            } else {
                $requests = Solicitud::with(['civil_status', 'house_type', 'spouse', 'type_document', 'references'])
                    ->get();
            }
            foreach ($requests as $reques) {
                unset($reques->imagen_persona);
                unset($reques->imagen_cedula_adelante);
                unset($reques->imagen_cedula_atras);
            }
            return response()->json($requests, 200);
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        if ($request->isJson() && $request->has('token')) {
            try {
                $fecha = new Carbon($request->personal['fecha_nacimiento']);
                $fecha_expedicion = new Carbon($request->personal['fecha_expedicion']);
                $solicitud = new Solicitud();
                $solicitud->fill($request->personal);
                $solicitud->fecha_nacimiento = $fecha;
                $solicitud->fecha_expedicion = $fecha_expedicion;
                $solicitud->vendedor = $request->vendedor;
                $solicitud->direcion = $request->localizacion['direcion'];
                $solicitud->barrio = $request->localizacion['barrio'];
                $solicitud->house_type_id = $request->localizacion['house_type_id'];
                $solicitud->telefono = $request->localizacion['telefono'];
                $solicitud->correo = $request->localizacion['correo'];
                $solicitud->salario = $request->laboral['salario'];
                $solicitud->otros_ingresos = $request->laboral['otros_ingresos'];
                $solicitud->procedencia = $request->laboral['procedencia'];
                $solicitud->imagen_persona = $this->RandomString(60);
                $solicitud->imagen_cedula_adelante = $this->RandomString(60);
                $solicitud->imagen_cedula_atras = $this->RandomString(60);
                if (isset($request->laboral['empresa'])) {
                    $servicio = new Carbon($request->laboral['tiempo_servicio']);
                    $solicitud->empresa = $request->laboral['empresa'];
                    $solicitud->profesion = $request->laboral['profesion'];
                    $solicitud->telefono_trabajo = $request->laboral['telefono_trabajo'];
                    $solicitud->direcion_empresa = $request->laboral['direcion_empresa'];
                    $solicitud->cargo = $request->laboral['cargo'];
                    $solicitud->tiempo_servicio = $servicio;
                    $solicitud->empleado = true;
                } else {
                    $solicitud->actividad_independiente = $request->laboral['actividad_independiente'];
                    $solicitud->empleado = false;
                }
                $solicitud->saveOrFail();
                if (count($request->conyuge) > 0) {
                    $conyuge = new Spouse();
                    $conyuge->fill($request->conyuge);
                    $conyuge->request_id = $solicitud->id;
                    $conyuge->saveOrFail();
                }
                $referencia = new Reference();
                $referencia1 = new Reference();
                $referencia->fill($request->referencia1);
                $referencia1->fill($request->referencia2);
                $referencia->saveOrFail();
                $referencia1->saveOrFail();
                $solicitud->references()->attach($referencia);
                $solicitud->references()->attach($referencia1);
                return response()->json(['message' => 'Solicitud agregada correctamente'], 200);
            } catch (ModelNotFoundException  $exception) {
                return response()->json(['message' => 'Ocurrio un error en la solicitud'], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /**
     * @param Request $request
     * @param Solicitud $solicitud
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Solicitud $solicitud)
    {
        if ($request->isJson() && $request->has('token')) {
            try {
                $solicitud = Solicitud::with(['civil_status', 'house_type', 'spouse', 'type_document', 'references'])
                    ->where('id', $request->id)
                    ->firstOrFail();
                return response()->json($solicitud, 200);
            } catch (ModelNotFoundException  $exception) {
                return response()->json(['message' => 'Ocurrio un error en la solicitud'], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Solicitud $solicitud
     * @return \Illuminate\Http\Response
     */
    public function edit(Solicitud $solicitud)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Solicitud $solicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Solicitud $solicitud)
    {
        if ($request->isJson() && $request->has('token')) {
            try {
                $solicitud = Solicitud::where('id', $request->id)->firstOrFail();
                $solicitud->estado = $request->status;
                $solicitud->saveOrFail();
                return response()->json(['message' => 'Cambio de estado correctamente'], 200);
            } catch (ModelNotFoundException  $exception) {
                return response()->json(['message' => 'Ocurrio un error en la solicitud'], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    public function updateRequest(Request $request)
    {
        if ($request->isJson() && $request->has('token')) {
            try {
                $solicitud = Solicitud::where('id', $request->id)->firstOrFail();
                $fecha = new Carbon($request->personal['fecha_nacimiento']);
                $fecha_expedicion = new Carbon($request->personal['fecha_expedicion']);
                $solicitud->nombre = $request->personal['nombre'];
                $solicitud->documento = $request->personal['documento'];
                $solicitud->lugar_expedicion = $request->personal['lugar_expedicion'];
                $solicitud->fecha_nacimiento = $fecha;
                $solicitud->fecha_expedicion = $fecha_expedicion;
                $solicitud->direcion = $request->localizacion['direcion'];
                $solicitud->barrio = $request->localizacion['barrio'];
                $solicitud->house_type_id = $request->localizacion['house_type_id'];
                $solicitud->telefono = $request->localizacion['telefono'];
                $solicitud->correo = $request->localizacion['correo'];
                $solicitud->salario = $request->laboral['salario'] === '' ? 0 : $request->laboral['salario'];
                $solicitud->otros_ingresos = $request->laboral['otros_ingresos'] === '' ? 0 : $request->laboral['otros_ingresos'];
                $solicitud->procedencia = $request->laboral['procedencia'];
                if (isset($request->laboral['empresa'])) {
                    $solicitud->empresa = $request->laboral['empresa'];
                    $solicitud->profesion = $request->laboral['profesion'];
                    $solicitud->telefono_trabajo = $request->laboral['telefono_trabajo'];
                    $solicitud->direcion_empresa = $request->laboral['direccion_empresa'];
                    $solicitud->cargo = $request->laboral['cargo'];
                    $solicitud->empleado = true;
                } else {
                    $solicitud->actividad_independiente = $request->laboral['actividad_independiente'];
                    $solicitud->empleado = false;
                }
                $referencia = Reference::where('id', $request->referencia1['id'])->firstOrFail();
                $referencia1 = Reference::where('id', $request->referencia2['id'])->firstOrFail();
                $referencia->nombre = $request->referencia1['nombre'];
                $referencia->direccion = $request->referencia1['direccion'];
                $referencia->telefono = $request->referencia1['telefono'];
                $referencia->ciudad = $request->referencia1['ciudad'];
                $referencia1->nombre = $request->referencia2['nombre'];
                $referencia1->direccion = $request->referencia2['direccion'];
                $referencia1->telefono = $request->referencia2['telefono'];
                $referencia1->ciudad = $request->referencia2['ciudad'];
                $solicitud->saveOrFail();
                $referencia->saveOrFail();
                $referencia1->saveOrFail();
                return response()->json(['message' => 'Actualizado correctamente'], 201);
            } catch (ModelNotFoundException  $exception) {
                return response()->json(['message' => 'Ocurrio un error en la solicitud'], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }

    private function RandomString($n)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < $n; $i++) {
            $randstring = $characters[rand(0, strlen($characters)-1)];
        }
        return $randstring;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Solicitud $solicitud
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(Solicitud $solicitud, Request $request)
    {
        if ($request->isJson() && $request->has('token')) {
            try {
                $solicitud = Solicitud::where('id', $request->id)->firstOrFail();
                $solicitud->delete();
                return response()->json(['message' => 'Eliminado correctamente  '], 200);
            } catch (ModelNotFoundException  $exception) {
                return response()->json(['message' => 'Ocurrio un error en la solicitud'], 500);
            }
        }
        return response()->json(['message' => 'Por favor autenticarse'], 401);
    }
}
