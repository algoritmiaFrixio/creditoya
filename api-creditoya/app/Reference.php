<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
    protected $hidden = ['crated_at', 'updated_at'];
    protected $fillable = ['nombre', 'direccion', 'telefono', 'ciudad'];
}
