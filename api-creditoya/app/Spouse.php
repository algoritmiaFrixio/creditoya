<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spouse extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['nombre_conyuge', 'numero_documento_conyuge', 'trabajo', 'telefono', 'salario', 'independiente', 'clase_actividad',];
}
