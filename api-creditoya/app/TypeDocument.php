<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeDocument extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
}
