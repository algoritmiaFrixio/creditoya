import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {InicioService} from '../shared/services/inicio.service';
import {ToastrService} from 'ngx-toastr';
import {TypeDocuments} from '../shared/interfaces/document/type-documents';
import {CivilStatus} from '../shared/interfaces/status/civil-status';
import {TypeHouse} from '../shared/interfaces/house/type-house';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {
  public accept = false;
  public checked = true;
  public conyuge = false;
  public firstFormGroup: FormGroup;
  public secondFormGroup: FormGroup;
  public thirstFormGroup: FormGroup;
  public fourstFormGroup: FormGroup;
  public fiveFormGroup: FormGroup;
  public sixFormGroup: FormGroup;
  public documents: TypeDocuments[];
  public status: CivilStatus[];
  public houses: TypeHouse[];

  public index = -1;
  public equalAddress = false;


  constructor(public dialog: MatDialog, private formBuilder: FormBuilder, private inicioService: InicioService,
              private toastr: ToastrService) {
    inicioService.getToken().subscribe(response => {
      inicioService.token = response.token;
    }, error1 => {
      toastr.error(error1.error.message);
    });
  }


  public change(direccion1, direccion2) {
    setTimeout(() => {
      this.equalAddress = (direccion1 === direccion2);
      if (this.equalAddress) {
        this.toastr.error('La dirección de las referencias no puede ser la misma');
      }
    });
  }

  // dialogo html
  openDialog(): void {
    const dialogRef = this.dialog.open(DialogAcceptTYCComponent, {
      width: '85vw',
      height: '65vh',
      disableClose: true,
      data: {accept: this.accept}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.accept = result;
    });
  }

  ngOnInit() {
    setTimeout(() => {
      this.openDialog();
    });
    this.inicioService.getDocument().subscribe((response: TypeDocuments[]) => {
      this.documents = response;
    }, error1 => {
      this.toastr.error(error1.error.message);
    });
    this.inicioService.getCivil().subscribe((response: CivilStatus[]) => {
      this.status = response;
    }, error1 => {
      this.toastr.error(error1.error.message);
    });
    this.inicioService.getHouse().subscribe((response: TypeHouse[]) => {
      this.houses = response;
    }, error1 => {
      this.toastr.error(error1.error.message);
    });
    this.changeWork();
    this.sixFormGroup = this.formBuilder.group({
      vendedor: ['Web']
    });
    this.secondFormGroup = this.formBuilder.group({
      direcion: ['', [Validators.required]],
      barrio: ['', [Validators.required]],
      house_type_id: ['', [Validators.required, Validators.pattern(/^([0-9])*$/)]],
      telefono: ['', [Validators.required, Validators.min(7)]],
      correo: ['', [Validators.pattern(/^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/)]],
    });
    this.firstFormGroup = this.formBuilder.group({
      nombre: ['', [Validators.required, Validators.pattern(/^([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\']+[\s])+([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])+[\s]?([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])?$/)]],
      type_document_id: ['', [Validators.required, Validators.pattern(/^([0-9])*$/)]],
      documento: ['', [Validators.required, Validators.pattern(/^([0-9])*$/)]],
      lugar_expedicion: ['', [Validators.required]],
      fecha_nacimiento: ['', [Validators.required]],
      fecha_expedicion: ['', [Validators.required]],
      civil_status_id: ['', [Validators.required, Validators.pattern(/^([0-9])*$/)]]
    });
    this.fiveFormGroup = this.formBuilder.group({
      nombre: ['', [Validators.required, Validators.pattern(/^([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\']+[\s])+([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])+[\s]?([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])?$/)]],
      direccion: ['', [Validators.required]],
      telefono: ['', [Validators.required, Validators.min(7)]],
      ciudad: ['', [Validators.required]],
      nombre1: ['', [Validators.required, Validators.pattern(/^([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\']+[\s])+([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])+[\s]?([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])?$/)]],
      direccion1: ['', [Validators.required]],
      telefono1: ['', [Validators.required, Validators.min(7)]],
      ciudad1: ['', [Validators.required]],

    });
  }

  public changeWork() {
    this.checked = !this.checked;
    if (this.checked) {
      this.thirstFormGroup = this.formBuilder.group({
        profesion: ['', [Validators.required]],
        empresa: ['', [Validators.required]],
        cargo: ['', [Validators.required]],
        direcion_empresa: ['', [Validators.required]],
        tiempo_servicio: ['', [Validators.required]],
        telefono_trabajo: ['', [Validators.required, Validators.min(7)]],
        salario: ['', [Validators.required, Validators.pattern(/^([0-9])*$/)]],
        otros_ingresos: ['', [Validators.pattern(/^([0-9])*$/)]],
        procedencia: [''],
      });
    } else {
      this.thirstFormGroup = this.formBuilder.group({
        actividad_independiente: ['', [Validators.required]],
        salario: ['', [Validators.required, Validators.pattern(/^([0-9])*$/)]],
        otros_ingresos: ['', [Validators.pattern(/^([0-9])*$/)]],
        procedencia: [''],
      });
    }
  }

  public changeStatus() {
    setTimeout(() => {
      this.conyuge = (this.firstFormGroup.value.civil_status_id === '2' || this.firstFormGroup.value.civil_status_id === '4') ||
        (this.firstFormGroup.value.civil_status_id === '7' || this.firstFormGroup.value.civil_status_id === '8');
      if (this.conyuge) {
        this.fourstFormGroup = this.formBuilder.group({
          nombre_conyuge: ['', [Validators.required, Validators.pattern(/^([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\']+[\s])+([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])+[\s]?([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])?$/)]],
          numero_documento_conyuge: ['', [Validators.required, Validators.pattern(/^([0-9])*$/)]],
          trabajo: [''],
          telefono: ['', [Validators.required, Validators.min(7)]],
          salario: ['', [Validators.pattern(/^([0-9])*$/)]],
          clase_actividad: [''],
        });
      } else {
        this.fourstFormGroup = this.formBuilder.group({});
      }
    });
  }

  public request(stepper) {
    {
      const data = {
        personal: {},
        localizacion: {},
        laboral: {},
        conyuge: {},
        referencia1: {},
        referencia2: {},
        token: this.inicioService.token,
        vendedor: this.sixFormGroup.value.vendedor,
      };
      data.personal = this.firstFormGroup.value;
      data.localizacion = this.secondFormGroup.value;
      data.laboral = this.thirstFormGroup.value;
      data.conyuge = this.fourstFormGroup.value;
      data.referencia1 = {
        nombre: this.fiveFormGroup.value.nombre,
        direccion: this.fiveFormGroup.value.direccion,
        telefono: this.fiveFormGroup.value.telefono,
        ciudad: this.fiveFormGroup.value.ciudad
      };
      data.referencia2 = {
        nombre: this.fiveFormGroup.value.nombre1,
        direccion: this.fiveFormGroup.value.direccion1,
        telefono: this.fiveFormGroup.value.telefono1,
        ciudad: this.fiveFormGroup.value.ciudad1
      };
      this.inicioService.createSolicitud(data).subscribe((response: any) => {
        this.inicioService.notification();
        this.toastr.success(response.message);
        this.firstFormGroup.reset();
        this.secondFormGroup.reset();
        this.thirstFormGroup.reset();
        this.fourstFormGroup.reset();
        this.fiveFormGroup.reset();
        this.sixFormGroup.reset();
        stepper.reset();
      }, error1 => {
        this.toastr.error(error1.error.message);
      });
    }
  }
}

@Component({
  selector: 'app-dialogaccepttyc',
  templateUrl: 'dialog.accept.tyc.html',
  styleUrls: ['./dialog.accept.tyc.scss']
})
export class DialogAcceptTYCComponent implements OnInit {
  tmp = false;

  constructor(
    public dialogRef: MatDialogRef<DialogAcceptTYCComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    document.getElementsByClassName('mat-dialog-container')[0].classList.add('dialogOpen');
  }

}
