import {Component, OnInit, ViewChild} from '@angular/core';
import {SwiperComponent, SwiperConfigInterface} from 'ngx-swiper-wrapper';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  public type = 'component';
  public config: SwiperConfigInterface = {
    a11y: true,
    loop: true,
    direction: 'horizontal',
    slidesPerView: 1,
    mousewheel: true,
    scrollbar: false,
    navigation: true,
    pagination: false,
    preloadImages: false,
    keyboard: {
      enabled: true,
      onlyInViewport: false,
    },
    autoplay: {
      delay: 6000,
    },
    // Enable lazy loading
    lazy: true
  };
  public service;
  // @ts-ignore
  @ViewChild(SwiperComponent, {static: false, read: false}) componentRef?: SwiperComponent;
  // @ts-ignore
  // @ts-ignore
  public swipper = [
    'assets/images/logos/pc0.png',
    'assets/images/logos/pc1.png',
    'assets/images/logos/pc2.png',
    'assets/images/logos/pc3.png',
    'assets/images/logos/pc4.png',
    'assets/images/logos/pc5.png',
    'assets/images/logos/pc6.png',
    'assets/images/logos/pc7.png',
  ];
  public swippermobile = [
    'assets/images/logos/mobile0.png',
    'assets/images/logos/mobile1.png',
    'assets/images/logos/mobile2.png',
    'assets/images/logos/mobile3.png',
    'assets/images/logos/mobile4.png',
    'assets/images/logos/mobile5.png',
    'assets/images/logos/mobile6.png',
    'assets/images/logos/mobile7.png',
  ];

  constructor() { }

  ngOnInit() {

  }

}
