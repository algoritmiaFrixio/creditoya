import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DialogAcceptTYCComponent, InicioComponent} from './inicio/inicio.component';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDialogModule,
  MatInputModule, MatListModule,
  MatNativeDateModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatStepperModule,
  MatToolbarModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ToastrModule} from 'ngx-toastr';
import {WebcamModule} from 'ngx-webcam';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {NgxUiLoaderModule, NgxUiLoaderRouterModule, NgxUiLoaderHttpModule} from 'ngx-ui-loader';
import {LandingPageComponent} from './landing-page/landing-page.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDividerModule} from '@angular/material/divider';
import {SWIPER_CONFIG, SwiperConfigInterface, SwiperModule} from 'ngx-swiper-wrapper';
import {AsyncPipe} from '@angular/common';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  observer: true,
  direction: 'horizontal',
  threshold: 50,
  spaceBetween: 5,
  slidesPerView: 1,
  centeredSlides: true
};


@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    DialogAcceptTYCComponent,
    LandingPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SwiperModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatToolbarModule,
    MatCheckboxModule,
    FormsModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    WebcamModule,
    NgxUiLoaderModule,
    NgxUiLoaderRouterModule.forRoot({showForeground: true}),
    NgxUiLoaderHttpModule.forRoot({showForeground: true}),
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    MatExpansionModule,
    MatDividerModule,
    MatListModule,
  ],
  providers:
    [
      MatDatepickerModule,
      AsyncPipe,
      {
        provide: SWIPER_CONFIG,
        useValue: DEFAULT_SWIPER_CONFIG
      }
    ],
  entryComponents: [DialogAcceptTYCComponent],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {


}
