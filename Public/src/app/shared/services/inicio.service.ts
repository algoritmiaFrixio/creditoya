import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {TypeDocuments} from '../interfaces/document/type-documents';
import {CivilStatus} from '../interfaces/status/civil-status';
import {TypeHouse} from '../interfaces/house/type-house';

@Injectable({
  providedIn: 'root'
})
export class InicioService {
  private _token = '';

  constructor(private http: HttpClient) {
  }

  get token(): string {
    return this._token;
  }

  set token(value: string) {
    this._token = value;
  }

  public getToken(): Observable<any> {
    return this.http.get<any>(`${environment.urlServe}`, {
      headers: {'Content-Type': 'apllication/json'}
    }).pipe(map((response: HttpResponse<any>) => response));
  }

  public getDocument(): Observable<TypeDocuments[]> {
    return this.http.get<TypeDocuments[]>(`${environment.urlServe}documento?token=${this._token}`, {
      headers: {'Content-Type': 'apllication/json'}
    }).pipe(map((response: TypeDocuments[]) => response));
  }

  public getHouse(): Observable<TypeHouse[]> {
    return this.http.get<TypeHouse[]>(`${environment.urlServe}tipo-casa?token=${this._token}`, {
      headers: {'Content-Type': 'apllication/json'}
    }).pipe(map((response: TypeHouse[]) => response));
  }

  public getCivil(): Observable<CivilStatus[]> {
    return this.http.get<CivilStatus[]>(`${environment.urlServe}estado-civil?token=${this._token}`, {
      headers: {'Content-Type': 'apllication/json'}
    }).pipe(map((response: CivilStatus[]) => response));
  }

  public createSolicitud(data): Observable<any> {
    return this.http.post(`${environment.urlServe}request`, data, {
      headers: {'Content-Type': 'application/json'}
    }).pipe(map((response: any) => response));
  }

  public getTokens(): Observable<any> {
    return this.http.get(`${environment.urlServe}token?token=${this._token}`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public notification() {
    this.getTokens().subscribe(response => {
      response.forEach(e => {
        const noti = {
          notification: {
            title: 'Nueva solicitud',
            body: 'Se ha ingresado una nueva solicitud',
            click_action: 'http://admincredito.almaceneselpunto.com/admin/solicitudes/En%20espera/todos'
          },
          to: e.token
        };
        this.Sendnotification(noti).subscribe(response2 => console.log(response2));
      });
    });

  }

  public Sendnotification(noti): Observable<any> {
    return this.http.post('https://fcm.googleapis.com/fcm/send', noti, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'key=AAAASqa__58:APA91bHr8H3JcVgViAy4pe3RF-RqlJbukKGUvcUQM7HzzwMadR2tWhRWBeyt2B1l8-NJqLbKaKbIGOj4wnyFMTeLDG6KTvbyASnxTisIDiGQXRSqXN26kkDsNLnt4K51oIN2Bhh5IoAs'
      }
    }).pipe(map(response => response));
  }

}
